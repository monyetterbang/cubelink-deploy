package com.cubelink.dto;

import java.util.Date;

public class QuizDTO {
	
	private Long seqId;
    private Date createdDt;
	private Date modifiedDt;
	
    private Long moduleId;
	
	private int totalMark;
	private String quizLevel;
	private int totQuizTakenByUser;
	
//	private List<QuizQuestDTO> quizQuestList ;
	
	private String quizTitle;
	private String quizDescription;
	
	private int totalQuestion;
	
	
	public Long getSeqId() {
		return seqId;
	}
	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}
	public Date getCreatedDt() {
		return createdDt;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public Date getModifiedDt() {
		return modifiedDt;
	}
	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}
	public Long getModuleId() {
		return moduleId;
	}
	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}
	public int getTotalMark() {
		return totalMark;
	}
	public void setTotalMark(int totalMark) {
		this.totalMark = totalMark;
	}
	public String getQuizLevel() {
		return quizLevel;
	}
	public void setQuizLevel(String quizLevel) {
		this.quizLevel = quizLevel;
	}
	public int getTotQuizTakenByUser() {
		return totQuizTakenByUser;
	}
	public void setTotQuizTakenByUser(int totQuizTakenByUser) {
		this.totQuizTakenByUser = totQuizTakenByUser;
	}
	public String getQuizTitle() {
		return quizTitle;
	}
	public void setQuizTitle(String quizTitle) {
		this.quizTitle = quizTitle;
	}
//	public List<QuizQuestDTO> getQuizQuestList() {
//		return quizQuestList;
//	}
//	public void setQuizQuestList(List<QuizQuestDTO> quizQuestList) {
//		this.quizQuestList = quizQuestList;
//	}
	public String getQuizDescription() {
		return quizDescription;
	}
	public void setQuizDescription(String quizDescription) {
		this.quizDescription = quizDescription;
	}
	public int getTotalQuestion() {
		return totalQuestion;
	}
	public void setTotalQuestion(int totalQuestion) {
		this.totalQuestion = totalQuestion;
	}
	
}
