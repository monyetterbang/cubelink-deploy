package com.cubelink.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.QuizQuestDTO;
import com.cubelink.entity.QuizQuest;

@Mapper
public interface QuizQuestMapper {

	public QuizQuestMapper INSTANCE = Mappers.getMapper(QuizQuestMapper.class);

	public QuizQuestDTO QuizQuestToQuizQuestDTO( QuizQuest m );
	
	public QuizQuestDTO QuizQuestToQuizQuestDTO( Optional<QuizQuest> m );
	
	public QuizQuest QuizQuestDTOtoQuizQuest( QuizQuestDTO m );
	
	public void updateQuizQuest ( QuizQuest user, @MappingTarget QuizQuest user1 );

	public void updateQuizQuest (QuizQuestDTO mDTO, @MappingTarget QuizQuest m );
}
