package com.cubelink.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "modules")
public class Module {
	
	/**
	 * 
	 */
	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME="modules_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="module_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="module_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long seqId;

	@Column (name="dcreatedt", nullable=false,updatable=true,insertable=true)
    private Date createdDt;
	
	@Column (name="smodtitle", nullable=false,updatable=true,insertable=true, length=255)
	private String moduleTitle;

	@Column (name="smoddesc", updatable=true,insertable=true, length=255)
	private String moduleDesc;

	@Column (name="smodfulldesc", updatable=true,insertable=true)
	private String fullModuleDesc;
	
	@Column (name="tmoddt", updatable=true,insertable=true)
    private Date modifiedDt;
	
	@Column(name = "slevel", insertable=true)
	private String moduleLevel;
	
	@Column (name="svideosrc", updatable=true,insertable=true)
	private String videoSrc;
	
	@Column (name="svideo", updatable=true,insertable=true)
	private String video;
	
	@Column (name="ilookcount", updatable=true,insertable=true)
	private int count;
	
	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getModuleTitle() {
		return moduleTitle;
	}

	public void setModuleTitle(String moduleTitle) {
		this.moduleTitle = moduleTitle;
	}

	public String getModuleDesc() {
		return moduleDesc;
	}

	public void setModuleDesc(String moduleDesc) {
		this.moduleDesc = moduleDesc;
	}

	public String getFullModuleDesc() {
		return fullModuleDesc;
	}

	public void setFullModuleDesc(String fullModuleDesc) {
		this.fullModuleDesc = fullModuleDesc;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getModuleLevel() {
		return moduleLevel;
	}

	public void setModuleLevel(String moduleLevel) {
		this.moduleLevel = moduleLevel;
	}

	public String getVideoSrc() {
		return videoSrc;
	}

	public void setVideoSrc(String videoSrc) {
		this.videoSrc = videoSrc;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	
}
