package com.cubelink.util;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class ClsUtils {

	private final Random random;

    public ClsUtils() {

        random = new Random();
    }

    public String generateRandomChars(String pattern, int length) {

        StringBuilder sb = new StringBuilder();


        return random.ints(0, pattern.length())
                .mapToObj(pattern::charAt)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    public Integer generateRandomInteger(Integer integer) {

        return random.ints(integer, 80)
                .findAny()
                .getAsInt();
    }
}
