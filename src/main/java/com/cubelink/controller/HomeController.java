package com.cubelink.controller	;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cubelink.dto.UserDTO;
import com.cubelink.entity.Constants;
import com.cubelink.entity.UserMaster;
import com.cubelink.manager.impl.UserManager;
import com.cubelink.mapper.UserMapper;
import com.cubelink.repo.UserRepo;

@Controller
@RequestMapping( value = "" )
public class HomeController { // extends BaseController {
	
	@Autowired
	private UserManager usManager;
	
	@Autowired
	private UserRepo uRepo;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@InitBinder("userDetail")
	public void initBinderUser(WebDataBinder binder) {
		// Auto convert empty string to null prior to controller entry
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, "userCreationDate", new CustomDateEditor(dateFormat, true));
	    binder.registerCustomEditor(Date.class, "userModifiedDate", new CustomDateEditor(dateFormat, true));

	}
	
	@ModelAttribute(value = "userDetail")
	public UserDTO initUserDTO() {
		return new UserDTO();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {Constants.MVC_INDEX, "" })
	public String baseContext() {
		return Constants.VIEW_INDEX;  
	}

	
	@RequestMapping(method = RequestMethod.GET, value = {Constants.MVC_LOGIN, "/login"} )
	public String login2(HttpSession session, Locale locale) throws Exception {
		
		if(session.getAttribute("username") == null) {
			return Constants.VIEW_LOGIN;
		}
		else {
			logout(session);
			return Constants.VIEW_LOGIN;
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = {Constants.MVC_LOGIN, "/login"} )
	public String auth2( HttpSession session, @ModelAttribute(value = "userLogin") UserDTO userDTO, ModelMap modelMap ) throws Exception {
		
		if (!userDTO.getUsername().isEmpty() && !userDTO.getPassword().isEmpty()) {
			
			logger.info("You've filled in all fields");

			if((usManager.authenticateUser(userDTO.getUsername(), userDTO.getPassword())) == true) {
				
				UserMaster um = usManager.retrieveByUserNP(userDTO.getUsername(), userDTO.getPassword()).get();
				if(um.getSessionCount()!= null) {
				 um.setSessionCount(um.getSessionCount()+1);
				} else {
					um.setSessionCount(1);
				}
				if(um.getUserLevel()== null) {
					 um.setUserLevel("B");
				}
				um = uRepo.saveAndFlush(um);
				
				UserDTO u = UserMapper.INSTANCE.UserToUserDTO(um);
				
				session.setAttribute("username", u.getUsername());
				session.setAttribute("password", u.getPassword());
				session.setAttribute("firstName", u.getFirstName());
				session.setAttribute("bio", u.getBio());
				session.setAttribute("id", u.getId());
				session.setAttribute("roleId", u.getRoleId());
				session.setAttribute("userLevel", u.getUserLevel());
				session.setAttribute("sessionCount", u.getSessionCount());
				session.setAttribute("point", u.getPoints());

				logger.info("Role Id of signed user: " + session.getAttribute("roleId") + " | " + session.getAttribute("userLevel") + " | session: "+um.getSessionCount());
				logger.info("Session: " +  u.getUsername() + " | " + u.getId());
				logger.info("You are logged in");
				
				String returnPathU = "redirect:/profile";
				String returnPathA = "redirect:/home";	
				logger.info("User Id logged in : " + u);

				if(Long.valueOf(session.getAttribute("roleId").toString())==1) {
					return returnPathA;	
				} else
					return returnPathU;
				
			}
			else {
				modelMap.put("error", "Invalid Account");

				logger.info("Authentication failed!");
				return "redirect:" + Constants.MVC_ERROR;
			}
		}
		else {
			logger.info("Please filled in all fields!");
			return "redirect:" + Constants.MVC_LOGIN;
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {Constants.MVC_SIGNUP, "/register"})
	public String register() {
		return Constants.VIEW_SIGNUP;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {Constants.MVC_LOGOUT, "/logout"})
	public String logout(HttpSession session) {
		if(session != null) {
			session.removeAttribute("username");
			session.removeAttribute("bio");
			session.removeAttribute("id");
			session.removeAttribute("password");
			session.removeAttribute("roleId");
			session.removeAttribute("firstName");
			session.removeAttribute("userLevel");

		}
			return "redirect:";
	}

	@RequestMapping(method = RequestMethod.GET, value = {Constants.MVC_ERROR, "/error"} )
	public String denied() {
		return Constants.VIEW_ERROR_ACCESS_DENIED;
	}
	
	
//	@RequestMapping(method = RequestMethod.GET, value = Constants.MVC_HOME )
//	public String home(Model model) {
//		model.addAttribute("chartFirstData", Arrays.asList(30, 50, 40, 61, 42, 35, 40));
//		model.addAttribute("chartSecondData", Arrays.asList(50, 40, 50, 40, 45, 40, 30));
//		
//		try {
//			Object dashboard = dbManager.getDashboardInformation( "2019", "FGC00001");
//			if ( dashboard == null ) {
//				dashboard = dbManager.getEmptyDashboard("2019", "FGC00001");
//			}
//			model.addAttribute("dash", dashboard );
//		}
//		catch ( Exception ex ) {
//			logger.error( ex.getMessage() );
//		}
//		
//		return Constants.VIEW_HOME;
//	}
//	
//	@RequestMapping(method = RequestMethod.GET, value = Constants.MVC_LOGIN )
//	public String login(Locale locale, ModelMap model) {
//		return Constants.VIEW_LOGIN;
//	}
//	
//	@RequestMapping(method = RequestMethod.POST, value = Constants.MVC_LOGIN )
//	public String auth(@ModelAttribute(value = "userDTO") UserDTO userDTO) {
//		return Constants.MVC_INDEX;
//	}
//	
//	@RequestMapping(method = RequestMethod.GET, value = Constants.MVC_ERROR )
//	public String denied() {
//		return Constants.VIEW_ERROR_ACCESS_DENIED;
//	}
	
}