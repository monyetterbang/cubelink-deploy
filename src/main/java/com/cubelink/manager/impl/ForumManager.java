package com.cubelink.manager.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cubelink.dto.ForumDTO;
import com.cubelink.dto.ForumRepliesDTO;
import com.cubelink.entity.Forum;
import com.cubelink.entity.ForumReplies;
import com.cubelink.manager.IForumManager;
import com.cubelink.mapper.ForumMapper;
import com.cubelink.mapper.ForumRepliesMapper;
import com.cubelink.repo.ForumRepliesRepo;
import com.cubelink.repo.ForumRepo;

@Service
public class ForumManager implements IForumManager {

	@Autowired
	private ForumRepo fRepo;
	
	@Autowired
	private ForumRepliesRepo rRepo;
	
	@Autowired
	private UserManager usManager;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	public List<Forum> listForum() {
		List<Forum> m = (List<Forum>) fRepo.listForumByLatest(); //.findAll();
		return m;	
	}
	
	public List<ForumReplies> listReplies(Long id) {
		List<ForumReplies> m = (List<ForumReplies>) rRepo.listRepliesByLatest(id); //.findAll();
		return m;	
	}
	
	public ForumDTO createNewForum (ForumDTO f, HttpSession s) throws Exception {
		Forum ff = newForum(f, s);
		return ForumMapper.INSTANCE.ForumToForumDTO(ff);
	}
	
	@Transactional
	private Forum newForum( ForumDTO u, HttpSession s ) throws Exception {
	    
		Forum um = ForumMapper.INSTANCE.ForumDTOtoForum(u);
		um.setCreatedDt(usManager.getCurrDate());
		um.setForumAuthor(Long.valueOf(s.getAttribute("id").toString()));
		um.setForumStatus("O");
		um.setReplies(0);
		
		um = fRepo.saveAndFlush(um);
		logger.info("[manager - newForum] New Forum created : " + um.getSeqId()+" | "+um.getForumTitle()+" at "+um.getCreatedDt());
    	return um;
	}
	
	public ForumRepliesDTO createNewReply(ForumRepliesDTO f, Long forumId, HttpSession s)throws Exception {
		ForumReplies pp = newReplies(f, forumId, s );
		return ForumRepliesMapper.INSTANCE.ForumRepliesToForumRepliesDTO(pp);
	}
	
	@Transactional
	private ForumReplies newReplies( ForumRepliesDTO f, Long id, HttpSession s ) throws Exception {
	    
		ForumReplies um = ForumRepliesMapper.INSTANCE.ForumRepliesDTOtoForumReplies(f);
		um.setCreatedDt(usManager.getCurrDate());
		um.setForumId(id);
		um.setAuthorName(s.getAttribute("username").toString());
		um.setAuthorId(Long.valueOf(s.getAttribute("id").toString()));
		
		Forum forum = fRepo.findBySeqId(id);
		forum.setReplies(fRepo.getNoOfReply(id)+1);
		forum = fRepo.saveAndFlush(forum);
		
		um = rRepo.saveAndFlush(um);
		logger.info("[New Reply - "+um.getForumId()+"] by : "+um.getAuthorName()+" | "+um.getAuthorId()+" at "+um.getCreatedDt());
		logger.info("[Total Reply for - " + um.getForumId() + "] is : " + forum.getReplies());

    	return um;
	}

}
