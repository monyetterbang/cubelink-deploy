package com.cubelink.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cubelink.dto.ModuleDTO;
import com.cubelink.dto.QuizDTO;
import com.cubelink.dto.QuizQuestDTO;
import com.cubelink.entity.Constants;
import com.cubelink.entity.Module;
import com.cubelink.entity.Quiz;
import com.cubelink.exception.RecordNotFoundException;
import com.cubelink.manager.impl.ModuleManager;
import com.cubelink.mapper.ModuleMapper;
import com.cubelink.mapper.QuizMapper;
import com.cubelink.repo.ModuleRepo;
import com.cubelink.repo.QuizRepo;

@Controller
@RequestMapping(value="/modules")
public class ModuleController {

	@Autowired
	private ModuleManager modManager;
	
	@Autowired 
	private ModuleRepo mRepo;
	
	@Autowired 
	private QuizRepo qRepo;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@InitBinder("modules")
	public void initBinderModules(WebDataBinder binder) {
		// Auto convert empty string to null prior to controller entry
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, "createdDt", new CustomDateEditor(dateFormat, true));
	    binder.registerCustomEditor(Date.class, "modifiedDt", new CustomDateEditor(dateFormat, true));
	}
	
	@InitBinder("quiz")
	public void initBinderQuiz(WebDataBinder binder) {
		// Auto convert empty string to null prior to controller entry
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, "createdDt", new CustomDateEditor(dateFormat, true));
	    binder.registerCustomEditor(Date.class, "modifiedDt", new CustomDateEditor(dateFormat, true));
	}
	@ModelAttribute(value = "modules")
	public ModuleDTO initModuleDTO() {
		return new ModuleDTO();
	}
	
	@ModelAttribute(value = "quiz")
	public QuizDTO initQuizDTO() {
		return new QuizDTO();
	}
	
	@ModelAttribute(value = "question")
	public QuizQuestDTO initQuest() {
		return new QuizQuestDTO();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String getModule() {
		return Constants.VIEW_MODULE_LIST;   
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/list" )
    public String findModules(Model model, HttpSession session) throws Exception {

        model.addAttribute("modules", modManager.listModuleByUserLevel(session.getAttribute("userLevel").toString())); 
        return Constants.VIEW_MODULE_LIST;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/quiz/list" )
    public String findQuiz(Model model, HttpSession session) throws Exception {

        model.addAttribute("quiz", modManager.findAllQuiz()); //session.getAttribute("userLevel").toString())); 
        return "views/quiz-list";
    }
	
	
	@RequestMapping( value="/new", method = RequestMethod.GET )
	public String createNewModule () {
        return Constants.VIEW_MODULE_NEW;
	}
	
	@RequestMapping( value="/new", method = RequestMethod.POST )
	public String createModule (@ModelAttribute("modules") ModuleDTO mdto, RedirectAttributes redirAttrs) {

		String returnPath = "redirect:/modules/list";
		logger.info("[createModule-fetched] : "+mdto.getSeqId()+" | "+mdto.getModuleTitle());

		try {
			ModuleDTO dto = modManager.createNewModule(mdto);
			logger.info("[createModule-saved] seqid = "+dto.getSeqId());
			redirAttrs.addFlashAttribute("msg", "Contact : " + dto.getSeqId() + " is saved successfully.");

			return returnPath;
		}catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
	}
	
	
	/* create new quiz*/
	@RequestMapping( value="/quiz/new", method = RequestMethod.GET )
	public String createNewQuiz () {
        return "views/quiz-new"; 
	}
	
	@RequestMapping( value="/quiz/new", method = RequestMethod.POST )
	public String createQuiz (@ModelAttribute("quiz") QuizDTO mdto, RedirectAttributes redirAttrs) {

		String returnPath = "redirect:/modules/quiz/list"; //+mdto.getSeqId();
		logger.info("[createQuiz-fetched] : "+mdto.getSeqId()+" | "+mdto.getQuizTitle());

		try {
			QuizDTO dto = modManager.createNewQuiz(mdto);
			logger.info("[createQuiz-saved] seqid = "+dto.getSeqId());
			redirAttrs.addFlashAttribute("msg", "Quiz : " + dto.getSeqId() + " is saved successfully.");
			
			return returnPath;
			
		}catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public String viewQuiz(@PathVariable("id") Long id,  @ModelAttribute("question") QuizQuestDTO mdto, @ModelAttribute("quiz") QuizDTO fDTO, Model model) throws RecordNotFoundException, Exception {
		
		Quiz m = qRepo.findBySeqId(id);
		logger.info("[viewQuiz] : "+m.getQuizTitle());
		
		model.addAttribute("quiz", QuizMapper.INSTANCE.QuizToQuizDTO( m ));  
		model.addAttribute("Quiz", fDTO);  
		model.addAttribute("question", modManager.listQuestions(id));
		
		return "views/quiz-view";
		
	}
	
	
	@RequestMapping( value="/quiz/quest/new_{id}", method = RequestMethod.POST )
	public String createReply (@PathVariable("id") Long quizId, @ModelAttribute("question") QuizQuestDTO mdto, Model model, HttpSession s, RedirectAttributes redirAttrs) {

		String returnPath = "redirect:/modules/quiz/"+quizId+"?";
		logger.info("[Quiz-fetched] : "+quizId+" | "+s.getAttribute("username").toString());

		try {
			QuizQuestDTO dto = modManager.createNewReply(mdto, quizId, s);
			logger.info("[addQuestion-saved] seqid = "+dto.getSeqId());
			redirAttrs.addFlashAttribute("msg", "Quiz_question : " + dto.getSeqId() + " is saved successfully.");

			return returnPath;
		}catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
	}
	
	
	@RequestMapping( value="/update/{id}", method = RequestMethod.GET )
	public String viewModule (@PathVariable("id") Long id, @ModelAttribute("modules") ModuleDTO mDTO, Model model, HttpSession session) {
		
		Module m = mRepo.findBySeqId(id);
		logger.info("[viewModule] : "+m.getModuleTitle()+" | count before: "+m.getCount());
		model.addAttribute("modules", ModuleMapper.INSTANCE.ModuleToModuleDTO( m )); 
		m.setCount(m.getCount()+1);
		
		if(Long.valueOf(session.getAttribute("roleId").toString())==1) {
	        return Constants.VIEW_MODULE_NEW;
		} else if (Long.valueOf(session.getAttribute("roleId").toString())!=1) {
			Module um = mRepo.saveAndFlush(m);
			logger.info("[viewModule] : "+m.getModuleTitle()+" | count after: "+um.getCount());
	        return "views/module_regular_view";
		}
		else 
			return Constants.VIEW_MODULE_LIST;
	}
	
	@RequestMapping( value="/quiz/update/{id}", method = RequestMethod.GET )
	public String viewQuiz (@PathVariable("id") Long id, @ModelAttribute("quiz") QuizDTO mDTO, Model model, HttpSession session) {
		
		Quiz m = qRepo.findBySeqId(id);
		logger.info("[viewQuiz] : "+m.getQuizTitle());
		model.addAttribute("quiz", QuizMapper.INSTANCE.QuizToQuizDTO( m )); 
		
		if(Long.valueOf(session.getAttribute("roleId").toString())==1) {
	        return "views/quiz-new"; 
		} else if (Long.valueOf(session.getAttribute("roleId").toString())!=1) {
	        return "views/Quiz_regular_view";
		}
		else 
			return "views/quiz-list"; 
	}
	
	@RequestMapping( value="/delete/{id}", method = RequestMethod.POST )
	public String deleteModule (@PathVariable("id") Long id, @ModelAttribute("modules") ModuleDTO mDTO, Model model, RedirectAttributes redirAttrs) {
		
		String returnPath = "redirect:/modules/list"; //Constants.VIEW_MODULE_LIST;
		Module m = mRepo.findBySeqId(id);
		try {
			Boolean exist = modManager.isModuleExist(m.getSeqId());
			logger.info("[Delete-existed] seqid = "+m.getSeqId());

			if (!exist) {
				redirAttrs.addFlashAttribute("msg", "Module is not exist.");
				return returnPath;
			}
			
			modManager.deleteModule(m.getSeqId());
			
			Module existAfterDelete = mRepo.findBySeqId(m.getSeqId());
			
			if (existAfterDelete == null) {
				logger.info("[Delete-module] module = "+m.getSeqId()+" is successful");	
			} else {
				logger.info("[Delete-module] module = "+m.getSeqId()+" is unsuccessful");	
			}

			return returnPath;
			
		} catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("modules") ModuleDTO m, RedirectAttributes redirAttrs, final Model model) {

    	String returnPath = "redirect:/modules/list";
		logger.info("[Update-fetched] title = "+m.getModuleTitle());
    	try {
			Boolean exist = modManager.isModuleExist( m.getSeqId() );
			logger.info("[Update-existed] seqid = "+m.getSeqId());
			if (!exist) {
				redirAttrs.addFlashAttribute("msg", "Update unsuccessful.");
				return returnPath;
			}
			modManager.updateModule(m);
			Module mod = mRepo.getOne(m.getSeqId());
			model.addAttribute("modules", ModuleMapper.INSTANCE.ModuleToModuleDTO( mod ));
			logger.info("[Update-updated] module = "+mod.getSeqId());
			redirAttrs.addFlashAttribute("msg", "Module No : " + mod.getSeqId() + " is saved successfully.");
			return returnPath;
		}
		catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
    }
	
	@RequestMapping(value = "/quiz/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("quiz") QuizDTO m, RedirectAttributes redirAttrs, final Model model) {

    	String returnPath = "redirect:/modules/quiz/list";
		logger.info("[Update Quiz-fetched] title = "+m.getQuizTitle());

    	try {

			Boolean exist = modManager.isModuleExist( m.getSeqId() );
			logger.info("[Update Quiz-existed] seqid = "+m.getSeqId());

			if (!exist) {
				redirAttrs.addFlashAttribute("msg", "Update unsuccessful.");
				return returnPath;
			}
			
			modManager.updateQuiz(m);
			Quiz mod = qRepo.getOne(m.getSeqId());
			model.addAttribute("quiz", QuizMapper.INSTANCE.QuizToQuizDTO( mod ));
			logger.info("[Update-updated] quiz = "+mod.getSeqId());

			redirAttrs.addFlashAttribute("msg", "Quiz No : " + mod.getSeqId() + " is saved successfully.");
			return returnPath;
		}
		catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
    }
	
	
	
	/*@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(@ModelAttribute("modules") ModuleDTO m, RedirectAttributes redirAttrs, final Model model) {

    	String returnPath = "redirect:/modules/list";
		logger.info("[Update-fetched] title = "+m.getModuleTitle());
    	try {
			Boolean exist = modManager.isUserExist( m.getSeqId() );
			logger.info("[Update-existed] seqid = "+m.getSeqId());
			if (!exist) {
				redirAttrs.addFlashAttribute("msg", "Update unsuccessful.");
				return returnPath;
			}
			modManager.updateModule(m);
			Module mod = mRepo.getOne(m.getSeqId());
			model.addAttribute("modules", ModuleMapper.INSTANCE.ModuleToModuleDTO( mod ));
			logger.info("[Update-updated] module = "+mod.getSeqId());
			redirAttrs.addFlashAttribute("msg", "Module No : " + mod.getSeqId() + " is saved successfully.");
			return returnPath;
		}
		catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
    }*/
}
