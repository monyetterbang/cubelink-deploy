create sequence dashboard_seq start 1 increment 1;
create sequence file_seq start 1 increment 1;
create sequence forum_seq start 1 increment 1;
create sequence modules_seq start 1 increment 1;
create sequence quiz_seq start 1 increment 1;
create sequence quizquest_seq start 1 increment 1;
create sequence replies_seq start 1 increment 1;
create sequence scmuser_seq start 1 increment 1;
create sequence scmuserrole_seq start 1 increment 1;

    create table dashboard (
       lseqid int8 not null,
        flatitude float8,
        flongitude float8,
        fpressure float8,
        ftemp float8,
        drealtime timestamp not null,
		user_lseqid_fk bigint,
        primary key (lseqid)
    );

    create table file (
       lseqid int8 not null,
        bfile bytea,
        sfilename varchar(255),
        sfiletype varchar(255),
        primary key (lseqid)
    );

    create table forum (
       lseqid int8 not null,
        dcreatedt timestamp not null,
        lauthor int8 not null,
        squestion varchar(255),
        status varchar(255) not null,
        stitle varchar(255),
        dlastreplydt timestamp,
        ireplies int4,
        primary key (lseqid)
    );

    create table forumreplies (
       lseqid int8 not null,
        lauthorid int8 not null,
        sauthorname varchar(255) not null,
        dcreatedt timestamp not null,
        lforumid int8 not null,
        sreply varchar(500) not null,
        primary key (lseqid)
    );

    create table modules (
       lseqid int8 not null,
        ilookcount int4,
        dcreatedt timestamp not null,
        smodfulldesc varchar(255),
        tmoddt timestamp,
        smoddesc varchar(255),
        slevel varchar(255),
        smodtitle varchar(255) not null,
        svideo varchar(255),
        svideosrc varchar(255),
        primary key (lseqid)
    );

    create table quizquest (
       lseqid int8 not null,
        dcreatedt timestamp,
        sanswer varchar(255),
        lquizid int8,
        squestion varchar(255),
        primary key (lseqid)
    );

    create table quizzes (
       lseqid int8 not null,
        dcreatedt timestamp,
        tmoddt timestamp,
        lmodid int8,
        squizdesc varchar(255),
        squizlevel varchar(255),
        squiztitle varchar(255),
        itotquiztaken int4,
        itotmark int4,
        itotquestion int4,
        primary key (lseqid)
    );

    create table scmuser (
       lseqid int8 not null,
        iage int4,
        sbio varchar(30),
        semail varchar(30),
        ienabled int4 not null,
        sfirstname varchar(30),
        slastname varchar(30),
        senpassword varchar(60) not null,
        ipoints int4,
        lroleid int8 not null,
        isession int4,
        dcreatedt timestamp,
        slevel varchar(255),
        tmoddt timestamp,
        susername varchar(60) not null,
        primary key (lseqid)
    );

    create table scmuserrole (
       roleid int8 not null,
        rolename varchar(30) not null,
        primary key (roleid)
    );
	
	alter table dashboard 
       add constraint dash_userid_fk 
       foreign key (user_lseqid_fk) 
       references scmuser;
       
    alter table forumreplies 
       add constraint forumreply_fk 
       foreign key (lforumid) 
       references scmuser,
	   add constraint reply_user_fk 
       foreign key (lauthorid) 
       references scmuser