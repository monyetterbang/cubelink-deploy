package com.cubelink.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.Quiz;

@Repository
public interface QuizRepo extends JpaRepository<Quiz, Long> {

	@Query(value = "select * from quizzes order by dcreatedt desc", nativeQuery=true)
	public List<Quiz> listQuizLatest();
	
	@Query(value = "select m.lseqid, m.dcreatedt, m.tmoddt, m.lmodid, m.itotmark, m.squizlevel, m.squizdesc, m.squiztitle, m.itotquiztaken from public.quiz m " 
			+ "inner join public.scmuser u on m.squizlevel = u.slevel "
			+ "where u.lseqid=?1 order by m.dcreatedt desc", nativeQuery=true)
	public List<Quiz> listQuizByUserLevel(Long id);
	
	@Query(value = "select * from quizzes where lseqid=?1", nativeQuery=true)
	public Quiz findBySeqId(Long id);

	@Query(value = "SELECT * FROM public.quizzes q inner join public.quizquest r "
			+ "on r.lquizid = q.lseqid where q.lseqid=?1 order by r.dcreatedt desc", nativeQuery=true)
	public List<Quiz> listQuestionByLatest(Long id);
	
	@Query(value = "SELECT COUNT(lseqid) " + 
			"FROM public.quizquest " + 
			"where lquizid=?1", nativeQuery=true)
	public int getNoOfQuestion(Long forumId);
	
	@Modifying
	@Transactional
	@Query( value = "delete from public.quizquest q " +
			"where q.lquizid=?1",
			nativeQuery=true )
	public void deleteQuestBySeqId (Long id);
}
