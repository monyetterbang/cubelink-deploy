package com.cubelink.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.QuizDTO;
import com.cubelink.entity.Quiz;

@Mapper
public interface QuizMapper {
	
	public QuizMapper INSTANCE = Mappers.getMapper(QuizMapper.class);

	public QuizDTO QuizToQuizDTO( Quiz m );
	
	public QuizDTO QuizToQuizDTO( Optional<Quiz> m );
	
	public Quiz QuizDTOtoQuiz( QuizDTO m );
	
	public void updateQuiz( Quiz user, @MappingTarget Quiz user1 );

	public void updateQuiz (QuizDTO mDTO, @MappingTarget Quiz m );
}
