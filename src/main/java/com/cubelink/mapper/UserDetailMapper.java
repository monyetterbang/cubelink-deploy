package com.cubelink.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.UserDetailDTO;
import com.cubelink.entity.UserDetail;

@Mapper
public interface UserDetailMapper {
	
	public UserDetailMapper INSTANCE = Mappers.getMapper(UserDetailMapper.class);

	public UserDetailDTO UserDetailToUserDetailDTO(UserDetail user);
	
	public UserDetail UserDetailDTOtoUserDetail( UserDetailDTO user );
	
	public void updateUserDetail( UserDetail user, @MappingTarget UserDetail user1 );

}
