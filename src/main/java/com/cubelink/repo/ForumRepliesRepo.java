package com.cubelink.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.ForumReplies;

@Repository
public interface ForumRepliesRepo extends JpaRepository<ForumReplies, Long> {

//	@Query(value = "SELECT r.lseqid, r.sreply, r.dcreatedt, r.lforumid, r.lauthorid FROM public.forum f inner join public.forumreplies r "
//			+ "on r.lforumid = f.lseqid where r.lforumid=?1 order by r.dcreatedt asc", nativeQuery=true)
//	public List<ForumReplies> listRepliesByLatest(Long id);

//	@Query(value = "SELECT r.lseqid, r.sreply, r.dcreatedt, r.lauthorid, r.lforumid, u.susername FROM public.forumreplies r " + 
//			"inner join public.forum f on f.lseqid = r.lforumid " + 
//			"inner join public.scmuser u on u.lseqid = r.lauthorid " + 
//			"where r.lforumid=?1 order by r.dcreatedt asc", nativeQuery=true)
	
	@Query(value = "SELECT r.lseqid, r.sreply, r.dcreatedt, r.lauthorid, u.susername, r.lforumid, " + 
			"(CASE WHEN r.sauthorname IS NULL THEN u.susername ELSE r.sauthorname END) AS sauthorname " + 
			"FROM public.forumreplies r " + 
			"inner join public.forum f on f.lseqid = r.lforumid " + 
			"inner join public.scmuser u on u.lseqid = r.lauthorid " + 
			"where r.lforumid=?1 order by r.dcreatedt desc", nativeQuery=true)
	public List<ForumReplies> listRepliesByLatest(Long id);
	
	@Query(value = "UPDATE public.forumreplies " + 
			"SET sauthorname = (" + 
			"        SELECT u.susername " + 
			"        FROM public.scmuser u " + 
			"        WHERE lauthorid = u.lseqid " + 
			"    ) ", nativeQuery=true)
	public ForumReplies updateAuthorNameReplies();
}
