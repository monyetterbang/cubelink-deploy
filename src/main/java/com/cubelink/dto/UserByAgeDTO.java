package com.cubelink.dto;

public class UserByAgeDTO {

	private Integer ageUnder18;
	private Integer age18To24;
	private Integer ageOver25;
	
	public Integer getAgeUnder18() {
		return ageUnder18;
	}
	public void setAgeUnder18(Integer ageUnder18) {
		this.ageUnder18 = ageUnder18;
	}
	public Integer getAge18To24() {
		return age18To24;
	}
	public void setAge18To24(Integer age18To24) {
		this.age18To24 = age18To24;
	}
	public Integer getAgeOver25() {
		return ageOver25;
	}
	public void setAgeOver25(Integer ageOver25) {
		this.ageOver25 = ageOver25;
	}
	
	
}
