package com.cubelink.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.UserMaster;

@Repository
public interface UserRepo extends  CrudRepository <UserMaster, Long>, JpaRepository<UserMaster, Long>, JpaSpecificationExecutor<UserMaster> {
	
//	@Query(value="select * from scmuser u" + 
//			" inner join scmuserdetail au on au.luserdetid=u.luserdetid" + 
//			" where u.lseqid = ?1", nativeQuery = true)
	public Optional<UserMaster> findById(Long seqId);
	
	@Query(value="select * from public.scmuser u" + 
			" where u.susername = ?1 and u.senpassword = ?2", nativeQuery = true)
	public UserMaster findByUsernameAndPassword(String username, String password);
	
	//@Query(value="select * from scmuser u where u.susername = ?1", nativeQuery = true)
	public Optional<UserMaster> findByUsername(String username);
	
	@Query(value="select * from public.scmuser u where u.susername=?1 and u.senpassword=?2", nativeQuery = true)
	public Optional<UserMaster> findByUsernamePass(String username, String pass);
	
	@Query(value=" SELECT SUM(CASE WHEN u.iage < 18 THEN 1 ELSE 0 END) AS \"[Under 18]\", " + 
			"SUM(CASE WHEN u.iage BETWEEN 18 AND 24 THEN 1 ELSE 0 END) AS \"[18-24]\", " + 
			"SUM(CASE WHEN u.iage BETWEEN 25 AND 34 THEN 1 ELSE 0 END) AS \"[25-34]\" " + 
			"FROM public.scmuser u", nativeQuery = true)
	public List<UserMaster> listUserByAge();
	
	@Query(value=" SELECT SUM(CASE WHEN u.iage < 18 THEN 1 ELSE 0 END) AS \"[Under 18]\" " + 
			"FROM public.scmuser u ", nativeQuery = true)
	public Integer listUserUnder18();
	
	@Query(value=" SELECT SUM(CASE WHEN u.iage BETWEEN 18 AND 24 THEN 1 ELSE 0 END) AS \"[18-24]\" " + 
			"FROM public.scmuser u", nativeQuery = true)
	public Integer listUser18to24();
	
	@Query(value=" SELECT SUM(CASE WHEN u.iage BETWEEN 25 AND 34 THEN 1 ELSE 0 END) AS \"[25-34]\" " + 
			"FROM public.scmuser u ", nativeQuery = true)
	public Integer listUserOver25();
	
	
	@Query(value=" SELECT SUM(CASE WHEN u.iage < 18 THEN 1 ELSE 0 END) AS \"[Under 18]\", " + 
			"SUM(CASE WHEN u.iage BETWEEN 18 AND 24 THEN 1 ELSE 0 END) AS \"[18-24]\", " + 
			"SUM(CASE WHEN u.iage BETWEEN 25 AND 34 THEN 1 ELSE 0 END) AS \"[25-34]\" " + 
			"FROM public.scmuser u", nativeQuery = true)
	public List<UserMaster> listUserByLevel();
	
	@Query(value=" SELECT SUM(CASE WHEN u.slevel = 'B' THEN 1 ELSE 0 END) AS \"[BEGIN]\" " + 
			"FROM public.scmuser u ", nativeQuery = true)
	public Integer listUserBeginner();
	
	@Query(value=" SELECT SUM(CASE WHEN u.slevel = 'I' THEN 1 ELSE 0 END) AS \"[INTER]\" " + 
			"FROM public.scmuser u", nativeQuery = true)
	public Integer listUserIntermediate();
	
	@Query(value=" SELECT SUM(CASE WHEN u.slevel = 'A' THEN 1 ELSE 0 END) AS \"[ADV]\" " + 
			"FROM public.scmuser u ", nativeQuery = true)
	public Integer listUserAdvance();
	
	@Query(value="SELECT * FROM public.scmuser where lroleid!=1 order by isession desc limit 3 ", nativeQuery=true)
	public List<UserMaster> mostActiveUser();
	
}
