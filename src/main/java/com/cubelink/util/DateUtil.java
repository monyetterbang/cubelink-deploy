package com.cubelink.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DateUtil {

public static Date convertStringtoDate( String strDate, DateTimeFormatter dtf ) throws Exception {
		
		LocalDate lDate = LocalDate.parse( strDate, dtf );
		return java.sql.Date.valueOf( lDate );
	}
	
	public static Integer durationBetween( Date startDate, Date endDate, DateTimeFormatter dtf ) {
		
		LocalDate date1 = LocalDate.parse( new SimpleDateFormat("ddMMyyyy").format( startDate ), dtf);
		LocalDate date2 = LocalDate.parse( new SimpleDateFormat("ddMMyyyy").format( endDate ), dtf );
		
		Long years = ChronoUnit.YEARS.between( date1, date2 );
		
		return years.intValue();
	}
	
	public static String convertDateToString( Date date, String formatter ) {

		SimpleDateFormat df = new SimpleDateFormat( formatter );
		
		return df.format( date );
	}
	
	public static Date addDayString( String strDate, Integer day, DateTimeFormatter dtf ) {
		
		LocalDate lDate = LocalDate.parse( strDate, dtf );
		LocalDate newDate = lDate.plusDays( day.longValue() );
		
		return java.sql.Date.valueOf( newDate );
	}
	
	public static Date addMonthString( String strDate, Integer month, DateTimeFormatter dtf ) {
	
		LocalDate lDate = LocalDate.parse( strDate, dtf );
		LocalDate newDate = lDate.plusMonths( month.longValue() );
		
		return java.sql.Date.valueOf( newDate );
	}
	
	
	public static Date moveDateByMonth( Integer month ) {
		
		DateFormat df = new SimpleDateFormat("ddMMyyyy");
		String date = df.format( new Date() );
		Date newDate = DateUtil.addMonthString( date, month, DateTimeFormatter.ofPattern("ddMMyyyy") );
		
		return newDate;
	}
	
	public static Date moveDateByDay( Integer day ) {
		
		DateFormat df = new SimpleDateFormat("ddMMyyyy");
		String date = df.format( new Date() );
		Date newDate = DateUtil.addDayString( date, day, DateTimeFormatter.ofPattern("ddMMyyyy") );
		
		return newDate;
	}
	
	public static Date getDateByWeek( Integer week, DayOfWeek dayofWeek ) {
		
		LocalDate localDate = LocalDate.now();
		localDate = localDate.plusWeeks( week );
		localDate = localDate.with(dayofWeek);
		
		return java.sql.Date.valueOf( localDate );
	}
	
	public static void main(String[] args) {
		
		/*Integer years = DateUtil.durationBetween( new Date(), new Date(), DateTimeFormatter.ofPattern("ddMMyyyy") );
		System.out.println( "Years : " + years );
		
		DateFormat df = new SimpleDateFormat("ddMMyyyy");
		String date = df.format( new Date() );
		Date futureDate = DateUtil.addMonthString( date, 1, DateTimeFormatter.ofPattern("ddMMyyyy") ); */
		
		Date startDate = DateUtil.getDateByWeek( -1, DayOfWeek.MONDAY );
		Date endDate = DateUtil.getDateByWeek( -1, DayOfWeek.SUNDAY );
		
		System.out.println( "Start Date : " + startDate );
		System.out.println( "End Date : " + endDate );
	}
}
