package com.cubelink.entity;

public final class Constants {

	private Constants() {
    	// restrict instantiation
    }
	
	// ==== Url Links ====
	public static final String MVC_BASE = "/web";
	public static final String MVC_INDEX = "/Index";
	public static final String MVC_HOME = "/Home";
	public static final String MVC_LOGIN = "/Login";
	public static final String MVC_SIGNUP = "/Register";
	public static final String MVC_LOGOUT = "/Logout";
	public static final String MVC_LOGIN_ERROR = "/Login?error";
	public static final String MVC_ERROR = "/access-denied";
	

	public static final String MVC_PROFILE = "/Profile";
	public static final String MVC_FORUM = "/Forum";
	public static final String MVC_MODULES = "/Modules";
	
	// + VIEW TEMPLATES +
	public static final String VIEW_INDEX = "views/index_";
	public static final String VIEW_HOME = "views/home";
	public static final String VIEW_SIGNUP = "views/signup";
	public static final String VIEW_LOGIN = "views/login";
	public static final String VIEW_ERROR_ACCESS_DENIED = "views/error/access-denied";
	
	

	public static final String VIEW_PROFILE = "views/profile";
	public static final String VIEW_MODULE = "views/modules";
	public static final String VIEW_FORUM = "views/forum";
	public static final String VIEW_MODULE_LIST = "views/module-list";
	public static final String VIEW_MODULE_NEW = "views/module-new";
}
