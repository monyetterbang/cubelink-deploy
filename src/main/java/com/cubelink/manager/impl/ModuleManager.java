package com.cubelink.manager.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.cubelink.dto.ModuleDTO;
import com.cubelink.dto.QuizDTO;
import com.cubelink.dto.QuizQuestDTO;
import com.cubelink.dto.ResponseDTO;
import com.cubelink.entity.Module;
import com.cubelink.entity.Quiz;
import com.cubelink.entity.QuizQuest;
import com.cubelink.exception.RecordNotFoundException;
import com.cubelink.manager.IModuleManager;
import com.cubelink.mapper.ModuleMapper;
import com.cubelink.mapper.QuizMapper;
import com.cubelink.mapper.QuizQuestMapper;
import com.cubelink.repo.ModuleRepo;
import com.cubelink.repo.QuizQuestRepo;
import com.cubelink.repo.QuizRepo;

@Service
public class ModuleManager implements IModuleManager {

	@Autowired
	private ModuleRepo mRepo;
	
	@Autowired
	private QuizRepo qRepo;
	
	@Autowired
	private QuizQuestRepo qqRepo;
	
	@Autowired
	private UserManager usManager;
	
	@Value("${cl.ws.modules.list}")
	private String WEB_SERVICE_MODULES_LIST;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@SuppressWarnings("unchecked")
	public List<ModuleDTO> getModuleLists( ) throws Exception { // String planType
		
		RestTemplate template = new RestTemplate();
		ResponseDTO response = template.getForObject( WEB_SERVICE_MODULES_LIST, ResponseDTO.class );
		
		return (List<ModuleDTO>)response.getReturnObj();
		
	}
	
	public List<Module> listModuleByUserLevel(String userLevel) {
		
		List<Module> ml = new LinkedList<Module>();
		
		if (userLevel.isEmpty()) {
			throw new RecordNotFoundException("[ listModuleByUserLevel ] Seq Id not found - ");
		}
		else {
			
			if(userLevel.contains("B")) {
				ml = (List<Module>) mRepo.listModuleByBeginner();
			} else if(userLevel.contains("I")) {
				ml = (List<Module>) mRepo.listModuleByIntermediate();
			} else if(userLevel.contains("A")) {
				ml = (List<Module>) mRepo.listModuleByAdvance();
			}
			
		}
		logger.info("List Module by User Level: "+ml);
		return ml;
		
	}
	
	//listQuizByUserLevel
	public List<Quiz> findAllQuiz() {
		List<Quiz> m = (List<Quiz>) qRepo.listQuizLatest(); 
		return m;
	}

	public List<QuizQuest> listQuestions(Long id) {
		List<QuizQuest> m = (List<QuizQuest>) qqRepo.listQuestionByLatest(id); //.findAll();
		return m;	
	}
	
	public List<Module> findAll() {

		List<Module> m = (List<Module>) mRepo.listModuleLatest(); //.findAll();
		return m;
	}

	public Boolean isModuleExist( Long seqId ) throws Exception {

		Optional<Module> value = mRepo.findById(seqId);
		logger.info("isModuleExist : "+value.get().getSeqId());
		return value.isPresent() ? true : false;
	}
	
	@Transactional
	private Module newModule( ModuleDTO u ) throws Exception {
	    
		Module um = ModuleMapper.INSTANCE.ModuleDTOtoModule(u);
		um.setCreatedDt(usManager.getCurrDate());
		um.setCount(0);
		
		um = mRepo.saveAndFlush(um);
		logger.info("[manager - newUser] UserMaster created : " + um.getSeqId()+" | "+um.getModuleTitle()+" at "+um.getCreatedDt());
    	return um;
	}
	
	public ModuleDTO createNewModule(ModuleDTO mDTO)throws Exception {
		Module pp = newModule (mDTO);
		return ModuleMapper.INSTANCE.ModuleToModuleDTO(pp);
	}
	
	@Transactional
	private Quiz newQuiz (QuizDTO u) throws Exception {
		
		Quiz um = QuizMapper.INSTANCE.QuizDTOtoQuiz(u);
		um.setCreatedDt(usManager.getCurrDate());
		
		um = qRepo.saveAndFlush(um);
		logger.info("[manager - newQuiz] Quiz created : " + um.getSeqId()+" | "+um.getQuizTitle()+" at "+um.getCreatedDt());
    	return um;
	}
	
	public QuizDTO createNewQuiz(QuizDTO mDTO)throws Exception {
		Quiz pp = newQuiz (mDTO);
		return  QuizMapper.INSTANCE.QuizToQuizDTO(pp);
	}

	@Transactional
	public ModuleDTO updateModule( ModuleDTO md ) throws Exception {

		md.setModifiedDt(usManager.getCurrDate());
		Optional<Module> entity = mRepo.findById( md.getSeqId() );
		Module m = entity.get();
		if(!entity.isPresent()) {
			throw new RecordNotFoundException("[ updateModule ] Seq Id not found -" + m.getSeqId());
		}
		
		ModuleMapper.INSTANCE.updateModule( md, m );
		m = mRepo.saveAndFlush( m );
		logger.info("[manager-updateModule]: " + m.getSeqId()+" | "+m.getModuleTitle()+" at "+m.getModifiedDt()+" | "+m.getCount());
		
		return ModuleMapper.INSTANCE.ModuleToModuleDTO(m);
	}
	
	@Transactional
	public QuizDTO updateQuiz( QuizDTO qD ) throws Exception {

		qD.setModifiedDt(usManager.getCurrDate());

		Optional<Quiz> entity = qRepo.findById( qD.getSeqId() );
		Quiz q = entity.get();
		if(!entity.isPresent()) {
			throw new RecordNotFoundException("[ updateQuiz ] Seq Id not found -" + qD.getSeqId());
		}
		QuizMapper.INSTANCE.updateQuiz( qD, q );
		q = qRepo.saveAndFlush( q );
		
		logger.info("[manager - updateUser] UserMaster updated : " + q.getSeqId()+" | "+q.getQuizTitle()+" at "+q.getModifiedDt());

		return QuizMapper.INSTANCE.QuizToQuizDTO(q);
	}
	
	public QuizQuestDTO createNewReply(QuizQuestDTO f, Long forumId, HttpSession s)throws Exception {
		QuizQuest pp = newReplies(f, forumId, s );
		return QuizQuestMapper.INSTANCE.QuizQuestToQuizQuestDTO(pp);
	}
	
	@Transactional
	private QuizQuest newReplies( QuizQuestDTO f, Long id, HttpSession s ) throws Exception {
	    
		QuizQuest um = QuizQuestMapper.INSTANCE.QuizQuestDTOtoQuizQuest(f);
		um.setCreatedDt(usManager.getCurrDate());
		um.setQuizId(id);
		
		Quiz forum = qRepo.findBySeqId(id);
		forum.setTotalMark(qRepo.getNoOfQuestion(id)+1); //.getNoOfReply(id)+1);
		forum = qRepo.saveAndFlush(forum);
		
		um = qqRepo.saveAndFlush(um);
		logger.info("[New Question - "+um.getQuizId()+"] at "+um.getCreatedDt());
		logger.info("[Total Question for - " + um.getQuizId() + "] is : " + forum.getTotalMark());

    	return um;
	}
	
//	@Transactional
//	public Quiz updateQuiz ( QuizDTO subDTO ) throws Exception {
//		subDTO.setModifiedDt(usManager.getCurrDate());
//		Quiz ss = QuizMapper.INSTANCE.QuizDTOtoQuiz(subDTO);
//		
//		if (ss.getSeqId() != null) {
//			removeList(ss.getSeqId());
//		}		
//		ss = setQuiz(ss);
//		return qRepo.saveAndFlush(ss);
//	}
	
	@Transactional
	private void removeList ( Long seqId ) throws Exception {
		qRepo.deleteQuestBySeqId(seqId);
	} 
	
//	private Quiz setQuiz ( Quiz prop ) throws Exception {
//		if (prop.getLifestyle() != null) {
//			if (prop.getSeqId() != null) {
//				prop.getLifestyle().setSeqId(prop.getSeqId());
//			}
//			prop.getLifestyle().setProposal(prop);
//		}
//		prop.getInsured().setProposal(prop);
//		if (prop.getPolicyHolder() != null) {
//			prop.getPolicyHolder().setProposal(prop);
//		}
//		for (int i = 0; i < prop.getQuizQuestList().size(); i++) {
//			prop.getQuizQuestList().get(i).setQuiz(prop);
//		}
//    	return prop;
//	}
	
	@Transactional
	public void deleteModule ( Long id ) throws Exception {
		mRepo.deleteById(id);
	}
		
}
