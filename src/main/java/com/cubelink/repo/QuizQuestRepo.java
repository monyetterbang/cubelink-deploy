package com.cubelink.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.QuizQuest;

@Repository
public interface QuizQuestRepo  extends JpaRepository<QuizQuest, Long> {

//	@Query(value = "SELECT r.lseqid, r.sreply, r.dcreatedt, r.lauthorid, u.susername, r.lforumid, " + 
//			"(CASE WHEN r.sauthorname IS NULL THEN u.susername ELSE r.sauthorname END) AS sauthorname " + 
//			"FROM public.forumreplies r " + 
//			"inner join public.forum f on f.lseqid = r.lforumid " + 
//			"inner join public.scmuser u on u.lseqid = r.lauthorid " + 
//			"where r.lforumid=?1 order by r.dcreatedt desc", nativeQuery=true)
	@Query(value = "SELECT * from public.quizquest where lquizid=?1 order by dcreatedt desc", nativeQuery=true)
	public List<QuizQuest> listQuestionByLatest(Long id);

}
