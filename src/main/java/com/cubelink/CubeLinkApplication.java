package com.cubelink;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication ( scanBasePackages={"com.cubelink.*"})
@EnableJpaRepositories("com.cubelink.repo") 	
@EntityScan("com.cubelink.entity")
@EnableJpaAuditing
@EnableScheduling
public class CubeLinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(CubeLinkApplication.class, args);
	}

}
