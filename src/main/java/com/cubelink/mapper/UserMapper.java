package com.cubelink.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.UserDTO;
import com.cubelink.entity.UserMaster;

@Mapper
public interface UserMapper {

	public UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

	public UserDTO UserToUserDTO( UserMaster user );
	
	public UserDTO UserToUserDTO( Optional<UserMaster> user );
	
	public UserMaster UserDTOtoMaster( UserDTO user );
	
	public void updateUserMaster( UserMaster user, @MappingTarget UserMaster user1 );

	public void updateUser(UserDTO uDTO, @MappingTarget UserMaster um);
	
//	public UserDetailDTO UserDetailToUserDetailDTO (UserDetail user);
//	
//	public UserDetail UserDetailDTOToUserDetail (UserDetailDTO user);
//
//	@Mapping(target = "seqId", ignore = true)
//	public void updateUserDetail( UserDetailDTO dto, @MappingTarget UserDetail factFind );

}
