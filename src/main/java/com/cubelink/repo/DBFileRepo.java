package com.cubelink.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.DBFile;

@Repository
public interface DBFileRepo extends JpaRepository<DBFile, String>{

}
