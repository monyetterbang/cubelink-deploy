package com.cubelink.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "quizzes")
public class Quiz {

	/**
	 * 
	 */
	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME="quiz_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="quiz_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="quiz_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long seqId;

	@Column (name="dcreatedt", updatable=true, insertable=true)
    private Date createdDt;
	
	@Column (name="tmoddt", updatable=true, insertable=true)
    private Date modifiedDt;
	
	@Column (name="lmodid", updatable=true, insertable=true)
	private Long moduleId;
	
	@Column (name="itotmark", updatable=true, insertable=true)
	private int totalMark;
	
	@Column (name="squizlevel", updatable=true, insertable=true)
	private String quizLevel;
	
	@Column (name="itotquiztaken", updatable=true, insertable=true)
	private int totQuizTakenByUser;

	@Column (name="squiztitle", updatable=true, insertable=true)
	private String quizTitle;
	
	@Column (name="squizdesc", updatable=true, insertable=true)
	private String quizDescription;
	
	@Column (name="itotquestion", updatable=true, insertable=true)
	private int totalQuestion;
	
//	@OneToMany ( mappedBy="quiz", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true )
//    private List<QuizQuest> quizQuestList ;
	
//	public List<QuizQuest> getQuizQuestList() {
//		return quizQuestList;
//	}
//
//	public void setQuizQuestList(List<QuizQuest> quizQuestList) {
//		this.quizQuestList = quizQuestList;
//	}

	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public Long getModuleId() {
		return moduleId;
	}

	public void setModuleId(Long moduleId) {
		this.moduleId = moduleId;
	}

	public int getTotalMark() {
		return totalMark;
	}

	public void setTotalMark(int totalMark) {
		this.totalMark = totalMark;
	}

	public String getQuizLevel() {
		return quizLevel;
	}

	public void setQuizLevel(String quizLevel) {
		this.quizLevel = quizLevel;
	}

	public int getTotQuizTakenByUser() {
		return totQuizTakenByUser;
	}

	public void setTotQuizTakenByUser(int totQuizTakenByUser) {
		this.totQuizTakenByUser = totQuizTakenByUser;
	}

	public String getQuizTitle() {
		return quizTitle;
	}

	public void setQuizTitle(String quizTitle) {
		this.quizTitle = quizTitle;
	}

	public String getQuizDescription() {
		return quizDescription;
	}

	public void setQuizDescription(String quizDescription) {
		this.quizDescription = quizDescription;
	}

	public int getTotalQuestion() {
		return totalQuestion;
	}

	public void setTotalQuestion(int totalQuestion) {
		this.totalQuestion = totalQuestion;
	}
	
}
