package com.cubelink.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ForumDTO {

	private Long seqId;
	private String forumTitle;
	private String forumQuestion;
    private Date modifiedDt;
    private String forumStatus;
    private int replies;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm a")
    private Date createdDt;

    private Long forumAuthorId;
    //private String forumAuthor;
    
    private ForumRepliesDTO reply;
    
	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public String getForumTitle() {
		return forumTitle;
	}

	public void setForumTitle(String forumTitle) {
		this.forumTitle = forumTitle;
	}

	public String getForumQuestion() {
		return forumQuestion;
	}

	public void setForumQuestion(String forumQuestion) {
		this.forumQuestion = forumQuestion;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getForumStatus() {
		return forumStatus;
	}

	public void setForumStatus(String forumStatus) {
		this.forumStatus = forumStatus;
	}

	public int getReplies() {
		return replies;
	}

	public void setReplies(int replies) {
		this.replies = replies;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getForumAuthorId() {
		return forumAuthorId;
	}

	public void setForumAuthorId(Long forumAuthorId) {
		this.forumAuthorId = forumAuthorId;
	}

	public ForumRepliesDTO getReply() {
		return reply;
	}

	public void setReply(ForumRepliesDTO reply) {
		this.reply = reply;
	}

//	public String getForumAuthor() {
//		return forumAuthor;
//	}
//
//	public void setForumAuthor(String forumAuthor) {
//		this.forumAuthor = forumAuthor;
//	}
	
}
