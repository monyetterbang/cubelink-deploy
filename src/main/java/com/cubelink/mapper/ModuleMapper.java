package com.cubelink.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.ModuleDTO;
import com.cubelink.entity.Module;

@Mapper
public interface ModuleMapper {

	public ModuleMapper INSTANCE = Mappers.getMapper(ModuleMapper.class);

	public ModuleDTO ModuleToModuleDTO( Module m );
	
	public ModuleDTO ModuleToModuleDTO( Optional<Module> m );
	
	public Module ModuleDTOtoModule( ModuleDTO m );
	
	public void updateModule( Module user, @MappingTarget Module user1 );

	public void updateModule (ModuleDTO mDTO, @MappingTarget Module m );
	
}
