package com.cubelink.dto;

import java.util.Date;

public class QuizQuestDTO {
	
	private Long seqId;

    private Date createdDt;
	
    private String quizQestion;
    private String quizAnswer;
	
    private Long quizId;
    
	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getQuizQestion() {
		return quizQestion;
	}

	public void setQuizQestion(String quizQestion) {
		this.quizQestion = quizQestion;
	}

	public String getQuizAnswer() {
		return quizAnswer;
	}

	public void setQuizAnswer(String quizAnswer) {
		this.quizAnswer = quizAnswer;
	}

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}

}
