package com.cubelink.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "scmuserdetail")
public class UserDetail {

	protected static final int ALLOC_SIZE=1;
	public static final String SEQUENCE_NAME2="scmuserdetail_seq";
	
	@Id
	@Column (name="luserdetid", nullable=false,insertable=true)
	@GeneratedValue(generator="userdet_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="userdet_seq", sequenceName=SEQUENCE_NAME2, allocationSize=ALLOC_SIZE)
	private Long seqId;
	
//	@OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
//	@JoinColumn(name = "luserdetid")
//	private UserMaster user;
	
//	@Column (name="sfirstname", updatable=true,insertable=true, length=60)
//	private String firstName;
//		
//	@Column (name="slastname",updatable=true,insertable=true, length=60)
//	private String lastName;
//		
//	@Column (name="semail", updatable=true,insertable=true, length=60)
//	private String emailAdd;
//		
//	@Column (name="dusercreatedt", updatable=true,insertable=true)
//	private Date userCreationDate;
//		
//	@Column (name="tmoddt", updatable=true,insertable=true)
//	private Date userModifiedDate;

//	@Column (name="saddress1", updatable=true,insertable=true, length=50)
//	private String address1;
//		
//	@Column (name="scity", updatable=true,insertable=true, length=30)
//	private String city;
//		
//	@Column (name="sstate", updatable=true,insertable=true, length=30)
//	private String state;
//		
//	@Column (name="scountry", updatable=true,insertable=true, length=20)
//	private String country;
//		
//	@Column (name="izipcode", updatable=true,insertable=true)
//	private Integer zipcode;
//	    
//	@Column (name="sfulladdress", updatable=true,insertable=true, length=100)
//	private String fullAddress;
		
//	@Column (name="sbio", updatable=true,insertable=true, length=50)
//	private String bio;
//		
//	@Column (name="sfullname", nullable=true, updatable=true,insertable=true, length=50)
//	private String fullName;

	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

}
