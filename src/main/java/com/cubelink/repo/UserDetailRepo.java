package com.cubelink.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.UserDetail;

@Repository
public interface UserDetailRepo extends CrudRepository <UserDetail, Long>, JpaRepository<UserDetail, Long>, JpaSpecificationExecutor<UserDetail> {
	
	//@Query(value="select u from scmuserdetail u where u.luserdetid = ?1")
	public UserDetail findBySeqId(Long seqId);
	
//    UserDetail findByFirstName(String firstName);
//
//    Boolean existsByFullName(String fullName);
//
//    Boolean existsByEmailAdd(String email);
    
//    @Query(value="SELECT ud.luserdetid, ud.sfirstname, ud.slastname, ud.semail, ud.dusercreatedt, ud.tmoddt, " + 
//			"ud.sfulladdress, ud.scity, ud.saddress1, ud.izipcode, ud.scountry, ud.sstate, ud.sbio  " + 
//			"FROM public.scmuser u " + 
//			"inner join scmuserdetail ud on ud.luserdetid=u.luserdetid " + 
//			"where ud.luserdetid=?1", nativeQuery=true)
	public UserDetail findUserDetailBySeqId(Long seqId);
}
