package com.cubelink.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.Forum;

@Repository
public interface ForumRepo extends JpaRepository<Forum, Long> {

	@Query(value = "select * from forum where lseqid=?1", nativeQuery=true)
	public Forum findBySeqId(Long id);
	
	@Query(value = "select * from forum order by dcreatedt desc", nativeQuery=true)
	public List<Forum> listForumByLatest();
	
	@Query(value = "SELECT * FROM public.forum f inner join public.forumreplies r "
			+ "on r.lforumid = f.lseqid where f.lseqid=?1 order by r.dcreatedt desc", nativeQuery=true)
	public List<Forum> listRepliesByLatest(Long id);
	
	@Query(value = "SELECT COUNT(lseqid) " + 
			"FROM public.forumreplies " + 
			"where lforumid=?1", nativeQuery=true)
	public int getNoOfReply(Long forumId);
}
