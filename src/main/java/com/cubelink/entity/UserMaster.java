package com.cubelink.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "scmuser")
public class UserMaster {
	
	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME="scmuser_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="user_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="user_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long id; 

	@Column (name="susername", nullable=false,updatable=true,insertable=true, length=60)
	private String username;
	
	@Column (name="senpassword", nullable=false,updatable=true,insertable=true, length=60)
    private String password;
    
	@Column(name = "lroleid", nullable=false)
	private Long roleId;
	
	@Column (name="ienabled", nullable=false,updatable=true,insertable=true)
    private Integer enabled;

//    @OneToOne ( mappedBy="user", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true )
//    private UserDetail userdetail;
    
    @OneToOne ( mappedBy="user", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true )
	private UserRole userrole;

    @Column (name="sfirstname", updatable=true,insertable=true, length=30)
	private String firstName;
		
	@Column (name="slastname",updatable=true,insertable=true, length=30)
	private String lastName;
		
	@Column (name="semail", updatable=true,insertable=true, length=30)
	private String emailAdd;
		
	@Column (name="dcreatedt", updatable=true,insertable=true)
	private Date userCreationDate;
		
	@Column (name="tmoddt", updatable=true,insertable=true)
	private Date userModifiedDate;
	
	@Column (name="sbio", updatable=true,insertable=true, length=30)
	private String bio;
	
	@Column (name="isession", updatable=true,insertable=true)
    private Integer sessionCount;
	
	@Column (name="iage", updatable=true,insertable=true)
    private Integer age;

	@Column(name = "slevel", updatable=true, insertable=true)
	private String userLevel;
	
	@Column(name = "ipoints", updatable=true, insertable=true)
	private Integer points;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public UserRole getUserrole() {
		return userrole;
	}

	public void setUserrole(UserRole userrole) {
		this.userrole = userrole;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAdd() {
		return emailAdd;
	}

	public void setEmailAdd(String emailAdd) {
		this.emailAdd = emailAdd;
	}

	public Date getUserCreationDate() {
		return userCreationDate;
	}

	public void setUserCreationDate(Date userCreationDate) {
		this.userCreationDate = userCreationDate;
	}

	public Date getUserModifiedDate() {
		return userModifiedDate;
	}

	public void setUserModifiedDate(Date userModifiedDate) {
		this.userModifiedDate = userModifiedDate;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public Integer getSessionCount() {
		return sessionCount;
	}

	public void setSessionCount(Integer sessionCount) {
		this.sessionCount = sessionCount;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

}
