#!/bin/bash
PID=`ps ax | grep ws-1.0.0-RELEASE.jar | grep -v grep | awk '{print $1}'`

if [ ! "${PID}" = "" ]
then
	kill -9 $PID
fi

java -d64 -Xms512m -Xmx1g -jar cl-1.0.0-RELEASE.jar --jasypt.encryptor.password=cubelinkEnc