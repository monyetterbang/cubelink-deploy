package com.cubelink.dto;

public class UserByLevelDTO {

	private Integer userBeginner;
	private Integer userInter;
	private Integer userAdv;
	
	public Integer getUserBeginner() {
		return userBeginner;
	}
	public void setUserBeginner(Integer userBeginner) {
		this.userBeginner = userBeginner;
	}
	public Integer getUserInter() {
		return userInter;
	}
	public void setUserInter(Integer userInter) {
		this.userInter = userInter;
	}
	public Integer getUserAdv() {
		return userAdv;
	}
	public void setUserAdv(Integer userAdv) {
		this.userAdv = userAdv;
	}
	
}
