package com.cubelink.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "forum")
public class Forum {
	
	/**
	 * 
	 */
	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME="forum_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="forum_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="forum_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long seqId;

	@Column (name="stitle",updatable=true,insertable=true, length=255)
	private String forumTitle;
	
	@Column (name="squestion",updatable=true,insertable=true, length=255)
	private String forumQuestion;
	
	@Column (name="dcreatedt", nullable=false,updatable=true,insertable=true)
    private Date createdDt;

	@Column (name="dlastreplydt", updatable=true,insertable=true)
    private Date modifiedDt;
	
	@Column (name="status", nullable=false,updatable=true,insertable=true)
    private String forumStatus;
	
	@Column (name="ireplies",updatable=true,insertable=true)
    private int replies;
	
	@Column (name="lauthor", nullable=false,updatable=true,insertable=true)
    private Long forumAuthor;
	
	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public String getForumTitle() {
		return forumTitle;
	}

	public void setForumTitle(String forumTitle) {
		this.forumTitle = forumTitle;
	}

	public String getForumQuestion() {
		return forumQuestion;
	}

	public void setForumQuestion(String forumQuestion) {
		this.forumQuestion = forumQuestion;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Date getModifiedDt() {
		return modifiedDt;
	}

	public void setModifiedDt(Date modifiedDt) {
		this.modifiedDt = modifiedDt;
	}

	public String getForumStatus() {
		return forumStatus;
	}

	public void setForumStatus(String forumStatus) {
		this.forumStatus = forumStatus;
	}

	public int getReplies() {
		return replies;
	}

	public void setReplies(int replies) {
		this.replies = replies;
	}

	public Long getForumAuthor() {
		return forumAuthor;
	}

	public void setForumAuthor(Long forumAuthor) {
		this.forumAuthor = forumAuthor;
	}
	
}
