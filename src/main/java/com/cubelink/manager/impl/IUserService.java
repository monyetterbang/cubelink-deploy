package com.cubelink.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cubelink.entity.UserMaster;
import com.cubelink.repo.UserRepo;

@Service
public class IUserService {
	
	@Autowired
    UserRepo uRepo;
    
//	@Autowired
//    UserDetailRepo uDetRepo;
    
    public UserMaster findOne( Long id ) {
        return uRepo.getOne(id);
    }
    
//    public UserDetail findDetOne( Long id ) {
//        return uDetRepo.getOne(id);
//    }
}
