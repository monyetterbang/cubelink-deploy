package com.cubelink.manager.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cubelink.dto.UserByAgeDTO;
import com.cubelink.dto.UserByLevelDTO;
import com.cubelink.dto.UserDTO;
import com.cubelink.entity.Module;
import com.cubelink.entity.UserMaster;
import com.cubelink.exception.RecordNotFoundException;
import com.cubelink.manager.IUserManager;
import com.cubelink.mapper.UserMapper;
import com.cubelink.repo.ModuleRepo;
import com.cubelink.repo.UserRepo;

@Service
public class UserManager implements IUserManager {
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private ModuleRepo mRepo;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/* ==================================================================
	 * 							- UserMaster - 
	 * ================================================================== */

//	public Page<UserDTO> retrieveAllUsers( Optional<Integer> optPage, Optional<Integer> optSize ) {
//		
//		int page, size;
//		
//		page = optPage.isPresent() ? optPage.get().intValue() : 0;
//		size = optSize.isPresent() ? optSize.get().intValue() : BaseSettings.ITEM_PER_PAGE;
//		
//		Pageable pageable = PageRequest.of( page, size, Direction.ASC, "userId");
//		Page<UserMaster> userPage = userRepo.findAll(pageable);
//		
//		return userPage.map(this::convertUserToUserDTO);
//	}
	
	public Optional<UserMaster> retrieveByUserNP( String userName, String pass ) throws Exception {
		
		Optional<UserMaster> user = userRepo.findByUsernamePass(userName, pass); //.findBySeqId( userName );

		if ( user == null ) {
			throw new Exception( "User Name not found!!" );
		}
		else {
			return user;//convertUserToUserDTO( user.get() );
		}
	}

	public UserDTO retrieveByUserNamePass( String userName, String pass ) throws Exception {
		
		Optional<UserMaster> user = userRepo.findByUsernamePass(userName, pass); //.findBySeqId( userName );

		if ( user == null ) {
			throw new Exception( "User Name not found!!" );
		}
		else {
			return convertUserToUserDTO( user.get() );
		}
	}
	
	public Boolean authenticateUser( String username,  String password ) {
		UserMaster user = userRepo.findByUsernameAndPassword( username, password );
		return ( user == null ) ? false : true;
	}
    
	public Boolean authenticate( Long seqId,  String password ) {
		Optional<UserMaster> user = userRepo.findById( seqId );
		return ( !user.isPresent() ) ? false : true;
	}
	
	public List<UserDTO> findByExample( UserMaster user ) {
		
		List<UserMaster> userList = userRepo.findAll(Example.of(user));
		List<UserDTO> dtoList = new ArrayList<UserDTO>();
		
		for ( int i = 0; i < userList.size(); i++ ) {
			UserMaster tmpUser = userList.get(i);
			UserDTO userDto = convertUserToUserDTO( tmpUser );
			dtoList.add( userDto );
		}
		
		return dtoList;
	}
	
	private UserDTO convertUserToUserDTO (UserMaster user) {
		UserDTO userDTO = UserMapper.INSTANCE.UserToUserDTO(user);
		return userDTO;
	}
	
	public UserDTO retrieveUserById(Long id) throws RecordNotFoundException, Exception {
		
		if ( id == null ) {
			throw new Exception ("Seq Id required! ");
		}
		
		Optional<UserMaster> entity = userRepo.findById( id );
		if (!entity.isPresent()) {
			throw new RecordNotFoundException("User Id not found - " + id );
		}
		return convertUserToUserDTO(entity.get());
	}

	public Boolean isUserExist( Long seqId ) throws Exception {

		Optional<UserMaster> value = userRepo.findById(seqId);
		System.out.println("isUserExist : "+value.get().getId());
		return value.isPresent() ? true : false;
	}
	
	public Boolean isUserNotExistByUsername( String username ) throws Exception {

		Optional<UserMaster> value = userRepo.findByUsername(username);
		logger.info("isUserExist : "+value.get().getUsername());
		return value.isPresent() ? false : true;
	}
	
	public UserDTO createNewUser( UserDTO u ) throws Exception {
		UserMaster pp = newUser(u);
		return UserMapper.INSTANCE.UserToUserDTO(pp);
	}
	
	@Transactional
	private UserMaster newUser( UserDTO u ) throws Exception {
      
		UserMaster um = UserMapper.INSTANCE.UserDTOtoMaster(u);
		if(userRepo.findAll()==null || userRepo.findAll().isEmpty()) {
			um.setRoleId(Long.valueOf(1));
			um.setUserLevel("A");
			um.setPoints(50);
			logger.info("You are the first to register as CubeLink member!");
		} else {
			um.setRoleId(Long.valueOf(2));	
			um.setPoints(5);
		}
		um.setEnabled(1);
		um.setUserCreationDate(getCurrDate()); //LocalDate.now().toDate());
		logger.info("[manager - newUser] UserMaster fetched : "+um.getUserCreationDate());

		um = userRepo.save(um);
		logger.info("[manager - newUser] UserMaster created : " + um.getId()+" | "+um.getUsername()+" at "+um.getUserCreationDate());
    	return um;
	}
	
	/***************** added on 27FEB ******************/
	
	@Transactional
	public UserDTO updateUser( UserDTO contactDTO ) throws Exception {

		contactDTO.setUserModifiedDate(getCurrDate()); //LocalDate.now().toDate());//java.sql.Date.valueOf(getCurrentTimeUsingCalendar()));

		Optional<UserMaster> entity = userRepo.findById( contactDTO.getId() );
		UserMaster contact = entity.get();
		if(!entity.isPresent()) {
			throw new RecordNotFoundException("[ updateProposal ] Seq Id not found -" + contactDTO.getId());
		}
		UserMapper.INSTANCE.updateUser( contactDTO, contact );
		contact = userRepo.saveAndFlush( contact );
		
		logger.info("[manager - updateUser] UserMaster updated : " + contact.getId()+" | "+contact.getUsername()+" at "+contact.getUserModifiedDate());

		return UserMapper.INSTANCE.UserToUserDTO(contact);
	}
	
	public UserDTO retrieveContactById( Optional<Long> id ) throws RecordNotFoundException, Exception {

		if ( !id.isPresent() ) {
			throw new Exception ("Proposal Id required! ");
		}
		
		Optional<UserMaster> entity = userRepo.findById(id.get());
		
		if (!entity.isPresent()) {
			throw new RecordNotFoundException("[ retrieveContactById ] Seq Id not found - " + id);
		}
		return convertUserToUserDTO(entity.get());

	}
	
	public String getCurrentTimeUsingCalendar() throws Exception {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        String formattedDate=dateFormat.format(date); 
		logger.info("[ModuleWS] Curr DateTIme: "+formattedDate+" | "+date);

		return formattedDate;
	}
	
	public Date getCurrDate() throws Exception {
        Date date = new Date();
		return date;
	}
	
	public UserByAgeDTO findAllByAge(UserByAgeDTO u) {

		//List<UserMaster> m = userRepo.listUserByAge();
		u.setAgeUnder18(userRepo.listUserUnder18());
		u.setAge18To24(userRepo.listUser18to24());
		u.setAgeOver25(userRepo.listUserOver25());

		return u;
	}
	
	public UserByLevelDTO findAllByLevel(UserByLevelDTO u) {

		//List<UserMaster> m = userRepo.listUserByAge();
		u.setUserBeginner(userRepo.listUserBeginner());
		u.setUserInter(userRepo.listUserIntermediate());
		u.setUserAdv(userRepo.listUserAdvance());

		return u;
	}
	
	public  List<UserMaster> getMostActiveUser() {

		List<UserMaster> u = userRepo.mostActiveUser();
		return u;
	}
	
	public  List<Module> getMostLookUpModule() {

		List<Module> u = mRepo.mostLookUpModule();
		return u;
	}
	
}
