package com.cubelink.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.Module;

@Repository
public interface ModuleRepo extends JpaRepository<Module, Long> {


	@Query(value = "select * from modules order by tmoddt desc", nativeQuery=true)
	public List<Module> listModuleLatest();
	
	@Query(value = "select m.lseqid, m.smodtitle, m.smoddesc, m.dcreatedt, m.smodfulldesc, m.tmoddt, m.slevel, m.svideosrc, m.svideo, m.ilookcount from public.modules m " 
			+ "inner join public.scmuser u on m.slevel = u.slevel "
			+ "where u.lseqid=?1 order by (CASE " 
			+ "WHEN m.tmoddt IS NULL THEN m.dcreatedt " 
			+ "ELSE m.tmoddt END) desc", nativeQuery=true)
	public List<Module> listModuleByUserLevel(Long id);
	
	@Query(value = "select * from modules where lseqid=?1", nativeQuery=true)
	public Module findBySeqId(Long id);
	
	@Query(value = "select * from modules where smodtitle=?1", nativeQuery=true)
	public Module findByModuleTitle(String title);
	
	@Query(value = "select m.lseqid, m.smodtitle, m.smoddesc, m.dcreatedt, m.smodfulldesc, m.tmoddt, m.slevel, m.svideosrc, m.svideo, m.ilookcount from public.modules m " 
			+ "where m.slevel='B' order by (CASE " 
			+ "WHEN m.tmoddt IS NULL THEN m.dcreatedt " 
			+ "ELSE m.tmoddt END) desc", nativeQuery=true)
	public List<Module> listModuleByBeginner();
	
	@Query(value = "select m.lseqid, m.smodtitle, m.smoddesc, m.dcreatedt, m.smodfulldesc, m.tmoddt, m.slevel, m.svideosrc, m.svideo, m.ilookcount from public.modules m " 
			+ "where m.slevel!='A' order by (CASE " 
			+ "WHEN m.tmoddt IS NULL THEN m.dcreatedt " 
			+ "ELSE m.tmoddt END) desc", nativeQuery=true)
	public List<Module> listModuleByIntermediate();
	
	@Query(value = "select m.lseqid, m.smodtitle, m.smoddesc, m.dcreatedt, m.smodfulldesc, m.tmoddt, m.slevel, m.svideosrc, m.svideo, m.ilookcount from public.modules m " 
			+ " order by (CASE " 
			+ "WHEN m.tmoddt IS NULL THEN m.dcreatedt " 
			+ "ELSE m.tmoddt END) desc ", nativeQuery=true)
	public List<Module> listModuleByAdvance();
	
	@Query(value = "select ilookcount from modules where lseqid=?1", nativeQuery=true)
	public Integer getModuleSessionCount(Long moduleId);
	
	@Query(value="SELECT * FROM public.modules order by ilookcount desc limit 3 ", nativeQuery=true)
	public List<Module> mostLookUpModule();
}
