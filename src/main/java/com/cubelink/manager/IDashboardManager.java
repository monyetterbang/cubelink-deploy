package com.cubelink.manager;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cubelink.entity.Dashboard;

public interface IDashboardManager {

	public Object getDashboardInformation( Date timestamp, String userId ) throws Exception;
	public List<Dashboard> findAllById(Long id);
	public Dashboard findLatestRecordById(Long id);	
	public Map<String, Object> getEmptyDashboard() throws Exception;
}
