package com.cubelink.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cubelink.dto.UserDTO;
import com.cubelink.entity.Constants;
import com.cubelink.entity.UserMaster;
import com.cubelink.manager.impl.IUserService;
import com.cubelink.manager.impl.UserManager;
import com.cubelink.mapper.UserMapper;
import com.cubelink.repo.UserRepo;

@Controller
@RequestMapping(value = "/")
public class UserController {
	
	@Autowired
	private UserManager usManager;
	
	@Autowired
	private IUserService uService;
	
	@Autowired
	private UserRepo uRepo;
	
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@InitBinder("userDetail")
	public void initBinderUser(WebDataBinder binder) {
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, "userCreationDate", new CustomDateEditor(dateFormat, true));
	    binder.registerCustomEditor(Date.class, "userModifiedDate", new CustomDateEditor(dateFormat, true));
	}
	@ModelAttribute(value = "userDetail")
	public UserDTO initUserDTO() {
		return new UserDTO();
	}

	@RequestMapping(value = "profile", method = RequestMethod.GET)
	public String viewProfile(HttpSession session, Model model) { //@PathVariable("id") Long id,
		
		UserMaster um = uService.findOne(Long.valueOf(session.getAttribute("id").toString()));
		
		logger.info("[viewProfile] User Profile fetched : " + um.getId() + " | " + um.getUsername());
		model.addAttribute("userDetail", UserMapper.INSTANCE.UserToUserDTO( um )); 

		return Constants.VIEW_PROFILE;
	}
	
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute("userDetail") UserDTO contactDTO, RedirectAttributes redirAttrs, final Model model) {

    	String returnPath = "redirect:/profile";
		logger.info("[Update-fetched] seqid = "+contactDTO.getId());

    	try {
    		Boolean exist = usManager.isUserExist( contactDTO.getId() );
			logger.info("[Update-existed] seqid = "+contactDTO.getId());

			if (!exist) {
				redirAttrs.addFlashAttribute("msg", "Update unsuccessful.");
				return returnPath;
			}
			
    		usManager.updateUser(contactDTO);
    		UserMaster contact = uService.findOne(contactDTO.getId());
			model.addAttribute("userDetail", UserMapper.INSTANCE.UserToUserDTO( contact ));
			logger.info("[Update-updated] seqid = "+contact.getId());

			redirAttrs.addFlashAttribute("msg", "Profile No : " + contactDTO.getId() + " is saved successfully.");
			return returnPath;
		}
		catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
    }
	
	@RequestMapping( value="new", method = RequestMethod.POST )
	public String createUser (@ModelAttribute("userDetail") UserDTO contactDTO, BindingResult result, RedirectAttributes redirAttrs) {

		String returnPath = "redirect:/Login";

		try {
			if(!uRepo.findByUsername(contactDTO.getUsername()).isPresent()) {
				UserDTO dto = usManager.createNewUser(contactDTO);
				logger.info("[createUser-saved] seqid = "+dto.getId()+" at "+dto.getUserCreationDate());
				redirAttrs.addFlashAttribute("msg", "Contact : " + dto.getId() + " is saved successfully.");
				logger.info("[setSessionDetails-after set] : "+dto.getId()+" | "+dto.getUsername());
		
				return returnPath;
			}
			else {
				redirAttrs.addFlashAttribute("msg", "Registration unsuccessful.");
				return "redirect:/Register";
			}
		}catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
	}
	
//	@SuppressWarnings({ "null", "unused" })
//	public HttpSession setSessionDetails (UserDTO u) {
//	
//		Long id = u.getId();
//		logger.info("[setSessionDetails-fetched] : "+u.getId()+" | "+u.getUsername());
//		HttpSession session = null;
//		if(session != null) {
//			session.removeAttribute("username");
//			session.removeAttribute("bio");
//			session.removeAttribute("id");
//			session.removeAttribute("password");
//			session.removeAttribute("roleId");
//			session.removeAttribute("firstName");
//		}
//		else {
//			session.setAttribute("id", id);
//			session.setAttribute("roleId", u.getRoleId());
//			session.setAttribute("username", u.getUsername());
//			session.setAttribute("password", u.getPassword());
//			session.setAttribute("firstName", u.getFirstName());
//			session.setAttribute("bio", u.getBio());
//		}
//		return session;
//	}
}
