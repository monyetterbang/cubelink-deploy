package com.cubelink.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "dashboard")
public class Dashboard {
	
	/**
	 * 
	 */
	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME="dashboard_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="dashboard_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="dashboard_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long seqId;
	
	@Column (name="ftemp", updatable=true,insertable=true)
	private double temperature;
	
	@Column (name="fpressure", updatable=true,insertable=true)
	private double pressure;

	@Column (name="flatitude", updatable=true,insertable=true)
	private double latitude;
	
	@Column (name="flongitude", updatable=true,insertable=true)
	private double longitude;
	
	@Column (name="drealtime", nullable=false,updatable=true,insertable=true)
	private Date timeStamp;

	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getPressure() {
		return pressure;
	}

	public void setPressure(double pressure) {
		this.pressure = pressure;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	
}