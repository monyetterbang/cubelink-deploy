package com.cubelink.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cubelink.entity.Dashboard;

@Repository
public interface DashboardRepo  extends JpaRepository<Dashboard, Long> {


	@Query(value = "SELECT * FROM public.dashboard where user_lseqid_fk=?1 order by drealtime desc", nativeQuery=true)
	public List<Dashboard> listDashboardLatest(Long id);
	
	@Query(value = "Select * from public.dashboard where user_lseqid_fk=?1 order by drealtime desc limit 1", nativeQuery=true)
	public Dashboard getDashboardLatest(Long id);
}
