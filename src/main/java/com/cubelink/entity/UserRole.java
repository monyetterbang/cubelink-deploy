package com.cubelink.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "scmuserrole")
public class UserRole {

	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME3="scmuserrole_seq";
	
	@Id
	@Column (name="roleid", nullable=false,updatable=true,insertable=true)
	@GeneratedValue(generator="roleId_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="roleId_seq", sequenceName=SEQUENCE_NAME3, allocationSize=ALLOC_SIZE)
	private Long roleId;
	
	@Column (name="rolename", nullable=false,updatable=true,insertable=true, length=30)
    private String roleName;

	@OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "roleid")
	private UserMaster user;
	
	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
