package com.cubelink.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "file")
public class DBFile {
	
	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME="file_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="file_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="file_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long seqId;

	@Column (name="sfilename", updatable=true, insertable=true)
    private String fileName;

	@Column (name="sfiletype", updatable=true, insertable=true)
    private String fileType;

    //@Lob //(name="sfiletype", updatable=true,insertable=true)
	@Lob
	@Type(type="org.hibernate.type.BinaryType")
	@Column (name="bfile", updatable=true, insertable=true)
    private byte[] bfile;

    public DBFile() {

    }

    public DBFile(String fileName, String fileType, byte[] bfile) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.bfile = bfile;
    }

	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getBfile() {
		return bfile;
	}

	public void setBfile(byte[] bfile) {
		this.bfile = bfile;
	}
    
    

}
