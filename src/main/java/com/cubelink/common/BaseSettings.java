package com.cubelink.common;

import org.springframework.http.MediaType;

public interface BaseSettings {

public static final String APPL_TYPE_JSON = MediaType.APPLICATION_JSON_VALUE;
	
	public static final String APPL_TYPE_XML = MediaType.APPLICATION_XML_VALUE;
	
	public static final String MULTITYPE_FORM = MediaType.MULTIPART_FORM_DATA_VALUE;
	
	public static final String APPL_TYPE_PDF = MediaType.APPLICATION_PDF_VALUE;

	// ==== Pagination ====
	public static final int ITEM_PER_PAGE = 10;
	public static final int DEFAULT_PAGE_NUMBER = 0;
	public static final int BUTTONS_TO_SHOW = 5;
	
	public static final String STRING_SORT_ASC = "ASC";
	public static final String STRING_SORT_DESC = "DESC";
	
	public static final String DEFAULT_FIELD = "seqId";
}
