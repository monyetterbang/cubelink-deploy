package com.cubelink.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cubelink.common.MediaTypeUtils;
import com.cubelink.dto.DashboardDTO;
import com.cubelink.dto.ModuleDTO;
import com.cubelink.dto.UserByAgeDTO;
import com.cubelink.dto.UserByLevelDTO;
import com.cubelink.dto.UserDTO;
import com.cubelink.entity.Constants;
import com.cubelink.manager.impl.DashboardManager;
import com.cubelink.manager.impl.UserManager;

@Controller
@RequestMapping( value = "" )
public class DashboardController { 
		
	@Autowired
	private UserManager usManager;
		
	@Autowired
	private DashboardManager dManager;
	
//	@Autowired
//	private UserService userService;
	
	@Autowired 
	private ServletContext context;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@InitBinder("dash")
	public void initBinderModules(WebDataBinder binder) {
		// Auto convert empty string to null prior to controller entry
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));	    
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, "dash.timeStamp", new CustomDateEditor(dateFormat, true));
	}
	
	@ModelAttribute(value = "dash")
	public DashboardDTO initDashboardDTO() {
		return new DashboardDTO();
	}

	@ModelAttribute(value = "latestDash")
	public DashboardDTO initLatestDashboardDTO() {
		return new DashboardDTO();
	}
	
	@ModelAttribute(value = "userByAge")
	public UserByAgeDTO initUserByAgeDTO() {
		return new UserByAgeDTO();
	}
	
	@ModelAttribute(value = "userByLevel")
	public UserByLevelDTO initUserByLevelDTO() {
		return new UserByLevelDTO();
	}
	
	@ModelAttribute(value = "activeUser")
	public UserDTO initUserDTO() {
		return new UserDTO();
	}
	
	@ModelAttribute(value = "activeModule")
	public ModuleDTO initModuleDTO() {
		return new ModuleDTO();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = {Constants.MVC_HOME, "/home"})
	public String index(HttpSession session, Model model) throws Exception {
		if(session.getAttribute("id").toString().isEmpty()) {
			logger.info("Your session has expired. You'll be logged out!");
			return "redirect:/logout";
		}
		else {
			logger.info("Dashboard - id fetched: "+session.getAttribute("id"));
			//model.addAttribute("dash", dManager.findAllById(Long.valueOf(session.getAttribute("id").toString())));
			
			try {
				Object dashboard = dManager.findAllById(Long.valueOf(session.getAttribute("id").toString()));
				if ( dashboard == null ) {
					dashboard = dManager.getEmptyDashboard();
				}
				else {
					dashboard = dManager.findLatestRecordById(Long.valueOf(session.getAttribute("id").toString()));
				}
				model.addAttribute("latestDash", dashboard );
			}
			catch ( Exception ex ) {
				logger.error( ex.getMessage() );
			}
				
			UserByAgeDTO u = new UserByAgeDTO();
			u = usManager.findAllByAge(u);
			logger.info("userByAge : " + u.getAge18To24() + " | "+u.getAgeOver25()+" | "+u.getAgeUnder18());
			model.addAttribute("userByAge", u);
			
			UserByLevelDTO l = new UserByLevelDTO();
			l = usManager.findAllByLevel(l);
			logger.info("userByLevel : " + l.getUserBeginner() + " | "+l.getUserInter()+" | "+l.getUserAdv());
			model.addAttribute("userByLevel", l);
			
			model.addAttribute("activeUser", usManager.getMostActiveUser());
			model.addAttribute("activeModule", usManager.getMostLookUpModule());
			

			return Constants.VIEW_HOME;
		}
	}
	
	 @RequestMapping("/download1")
	 public ResponseEntity<InputStreamResource> downloadFile1(
	            @RequestParam(defaultValue = "fileName") String fileName) throws IOException {
	 
	        MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.context, fileName);
	        System.out.println("fileName: " + fileName);
	        System.out.println("mediaType: " + mediaType);
	 
	        File file = new File("/assets/filedata" + "/" + fileName);
	        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
	 
	        return ResponseEntity.ok()
	                // Content-Disposition
	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
	                // Content-Type
	                .contentType(mediaType)
	                // Contet-Length
	                .contentLength(file.length()) //
	                .body(resource);
	}
	 
	@RequestMapping(method= RequestMethod.GET, value="/home/getFile/{id}")
	public String downloadExcel(@PathVariable(value="id") String filename) {

			logger.info("Filename to download: "+filename);
			String filePath = "redirect:/assets/filedata/"+filename;
			//filedownload(filePath, response, filename);
			
			logger.info("filedownloading... "+filename);
			return filePath;
	}
	 
	@RequestMapping(method= RequestMethod.GET, value="/createExcel/{id}")
	public void downloadExcel(@PathVariable(value="id") String filename, 
			HttpServletRequest request, HttpServletResponse response) {

		logger.info("Filename to download: "+filename);
		String filePath = request.getServletContext().getRealPath("/assets/filedata/"+filename);
		filedownload(filePath, response, filename);
		
		logger.info("filedownload... "+filename);
	}
	
	public void filedownload(String fullPath, HttpServletResponse response, String filename) {
		
		File file = new File(fullPath);
		final int BUFFER_SIZE = 4096;
		if(file.exists()) {
			try {
				FileInputStream is = new FileInputStream(file);
				String mimeType = context.getMimeType(fullPath);
				response.setContentType(mimeType);
				response.setHeader("content-disposition", "attachment; filename="+filename);
				
				OutputStream os = response.getOutputStream();
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
				while ((bytesRead=is.read(buffer))!=-1) {
					os.write(buffer, 0, bytesRead);
					logger.info("filedownload... "+os.toString());
				}
				is.close();
				os.close();
				file.delete();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Handle request to download an Excel document
	 */
//	@RequestMapping(value = "/download", method = RequestMethod.GET)
//	public String download(Model model) {
//	    model.addAttribute("users", userService.findAllUsers());
//	    return "/home";
//	}
	
//	@RequestMapping(method = RequestMethod.GET, value = Constants.MVC_HOME )
//	public String home(Model model) {
//		model.addAttribute("chartFirstData", Arrays.asList(30, 50, 40, 61, 42, 35, 40));
//		model.addAttribute("chartSecondData", Arrays.asList(50, 40, 50, 40, 45, 40, 30));
//		
//		try {
//			Object dashboard = dbManager.getDashboardInformation( "2019", "FGC00001");
//			if ( dashboard == null ) {
//				dashboard = dbManager.getEmptyDashboard("2019", "FGC00001");
//			}
//			model.addAttribute("dash", dashboard );
//		}
//		catch ( Exception ex ) {
//			logger.error( ex.getMessage() );
//		}
//		
//		return Constants.VIEW_HOME;
//	}
}
