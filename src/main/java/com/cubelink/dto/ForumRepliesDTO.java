package com.cubelink.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ForumRepliesDTO {
	
	private Long seqId;
	private String forumReply;
	private Long forumId;
	private Long authorId;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm a")
    private Date createdDt;
 
	private String authorName;
	
	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public String getForumReply() {
		return forumReply;
	}

	public void setForumReply(String forumReply) {
		this.forumReply = forumReply;
	}

	public Long getForumId() {
		return forumId;
	}

	public void setForumId(Long forumId) {
		this.forumId = forumId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
}
