package com.cubelink.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DashboardDTO {
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MMM/yyyy HH:mm a")
    private Date timeStamp;
	
	private Long seqId;
    private double temperature;
    private double pressure;
    private double latitude;
    private double longitude;
    
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public Long getSeqId() {
		return seqId;
	}
	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}
	public double getTemperature() {
		return temperature;
	}
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	public double getPressure() {
		return pressure;
	}
	public void setPressure(double pressure) {
		this.pressure = pressure;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
    
    
}
