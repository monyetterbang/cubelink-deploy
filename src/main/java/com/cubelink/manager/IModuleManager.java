package com.cubelink.manager;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.cubelink.dto.ModuleDTO;
import com.cubelink.dto.QuizDTO;
import com.cubelink.dto.QuizQuestDTO;
import com.cubelink.entity.Module;
import com.cubelink.entity.Quiz;
import com.cubelink.entity.QuizQuest;

public interface IModuleManager {

	public List<ModuleDTO> getModuleLists( ) throws Exception;  // String planType

	List<Module> findAll();
	
	//added 1June
	public List<Module> listModuleByUserLevel(String userLevel);

	//added 27Feb 4pm
	public ModuleDTO createNewModule(ModuleDTO mDTO)throws Exception;
	public ModuleDTO updateModule( ModuleDTO contactDTO ) throws Exception;

	public Boolean isModuleExist( Long seqId ) throws Exception;
	
	public void deleteModule ( Long id ) throws Exception;
	
	public List<Quiz> findAllQuiz();
	
	public QuizDTO createNewQuiz(QuizDTO mDTO) throws Exception;
	
//	public Quiz updateQuiz ( QuizDTO subDTO ) throws Exception;
//	public List<Quiz> listQuestions(Long id);
	public List<QuizQuest> listQuestions(Long id);

	public QuizDTO updateQuiz( QuizDTO qD ) throws Exception;
	
	public QuizQuestDTO createNewReply(QuizQuestDTO f, Long forumId, HttpSession s)throws Exception;

}
