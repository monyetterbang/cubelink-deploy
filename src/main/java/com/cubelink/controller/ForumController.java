package com.cubelink.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cubelink.dto.ForumDTO;
import com.cubelink.dto.ForumRepliesDTO;
import com.cubelink.dto.UserDTO;
import com.cubelink.entity.Constants;
import com.cubelink.entity.Forum;
import com.cubelink.exception.RecordNotFoundException;
import com.cubelink.manager.impl.ForumManager;
import com.cubelink.manager.impl.UserManager;
import com.cubelink.mapper.ForumMapper;
import com.cubelink.repo.ForumRepo;

@Controller
@RequestMapping(value={Constants.MVC_FORUM, "/forum"})
public class ForumController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ForumRepo fRepo;
	
	@Autowired
	private ForumManager forumManager;
	
	@Autowired
	private UserManager usManager;
	
	@ModelAttribute(value = "forum")
	public ForumDTO initForumDTO() {
		return new ForumDTO();
	}
	
	@ModelAttribute(value = "Forum")
	public ForumDTO initForumDTO2() {
		return new ForumDTO();
	}
	
	@ModelAttribute(value = "user")
	public UserDTO initUserDTO() {
		return new UserDTO();
	}
	
	@ModelAttribute(value = "replies")
	public ForumRepliesDTO initForumRepliesDTO() {
		return new ForumRepliesDTO();
	}
	
	@ModelAttribute(value = "reply")
	public ForumRepliesDTO initForumRepliesDTO2() {
		return new ForumRepliesDTO();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String getForum(Model model, HttpSession session) throws Exception {
		
		model.addAttribute("forum", forumManager.listForum());

		return Constants.VIEW_FORUM;  
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "{id}")
	public String viewForum(@PathVariable("id") Long id,  @ModelAttribute("reply") ForumRepliesDTO mdto, @ModelAttribute("Forum") ForumDTO fDTO, Model model) throws RecordNotFoundException, Exception {
		
		Forum m = fRepo.findBySeqId(id);
		logger.info("[viewForum] : "+m.getForumTitle());
		
		UserDTO ud = usManager.retrieveUserById(m.getForumAuthor());
		logger.info("Forum Author: " + ud.getUsername());
		
		model.addAttribute("forum", ForumMapper.INSTANCE.ForumToForumDTO( m ));  
		model.addAttribute("Forum", fDTO);  
		model.addAttribute("user", ud);
		model.addAttribute("replies", forumManager.listReplies(id));
		model.addAttribute("reply", mdto);
		
		return "views/forum-view";
		
	}
	
	@RequestMapping( value="/new", method = RequestMethod.POST )
	public String newForum (@ModelAttribute("forum") ForumDTO fdto, RedirectAttributes redirAttrs, HttpSession s) {

		String returnPath = "redirect:/forum/";
		logger.info("[newForum-fetched] : "+fdto.getSeqId()+" | "+fdto.getForumTitle());

		try {
			ForumDTO dto = forumManager.createNewForum(fdto, s);
			logger.info("[createForum-saved] "+dto.getSeqId()+" = "+dto.getForumTitle());
			redirAttrs.addFlashAttribute("msg", "Forum : " + dto.getSeqId() + " is saved successfully.");

			return returnPath;
			
		}catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
	}
	
	@RequestMapping( value="/rep/new_{id}", method = RequestMethod.POST )
	public String createReply (@PathVariable("id") Long forumId, @ModelAttribute("replies") ForumRepliesDTO mdto, Model model, HttpSession s, RedirectAttributes redirAttrs) {

		String returnPath = "redirect:/forum/"+forumId+"?";
		logger.info("[forum-fetched] : "+forumId+" | "+s.getAttribute("username").toString());

		try {
			ForumRepliesDTO dto = forumManager.createNewReply(mdto, forumId, s);
			logger.info("[addReply-saved] seqid = "+dto.getSeqId());
			redirAttrs.addFlashAttribute("msg", "Contact : " + dto.getSeqId() + " is saved successfully.");

			return returnPath;
		}catch (Exception ex) {
			logger.error( ex.getMessage(), ex );
			redirAttrs.addFlashAttribute("msg", ex.getMessage());
			return returnPath;
		}
	}
}
