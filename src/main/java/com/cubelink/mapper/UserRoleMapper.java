package com.cubelink.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.UserRoleDTO;
import com.cubelink.entity.UserRole;

@Mapper
public interface UserRoleMapper {

	public UserRoleMapper INSTANCE = Mappers.getMapper(UserRoleMapper.class);

	public UserRoleDTO UserRoleToUserRoleDTO( UserRole user );
	
	public UserRoleDTO UserRoleToUserRoleDTO( Optional<UserRole> user );
	
	public UserRole UserRoleDTOtoUserRole( UserRoleDTO user );
	
	public void updateUserRole( UserRole user, @MappingTarget UserRole user1 );

	public void updateUserRole(UserRoleDTO uDTO, @MappingTarget UserRole um);
}