package com.cubelink.manager.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cubelink.dto.DBBoxDTO;
import com.cubelink.dto.ResponseDTO;
import com.cubelink.entity.Dashboard;
import com.cubelink.manager.IDashboardManager;
import com.cubelink.repo.DashboardRepo;

@Service
public class DashboardManager implements IDashboardManager {
	
//	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DashboardRepo dRepo;
	
    @Value("${cl.ws.dashboard.url}")
    private String WEB_SERVICE_DASHBOARD_URL;
	
	public Object getDashboardInformation( Date timestamp, String userId ) throws Exception {
		
		Object[] params = { timestamp, userId };
		RestTemplate template = new RestTemplate();
		ResponseDTO response = template.getForObject( WEB_SERVICE_DASHBOARD_URL, ResponseDTO.class, params );
		if ( response.getReturnObj() == null ) {
			return null;
		}
		else {
			return response.getReturnObj();
		}
	}
	
	public List<Dashboard> findAllById(Long id) {
		
		List<Dashboard> m = (List<Dashboard>) dRepo.listDashboardLatest(id); 
		return m;
	}
	
	public Dashboard findLatestRecordById(Long id) {
		
		Dashboard m = dRepo.getDashboardLatest(id); 
		return m;
	}
	
	public Map<String, Object> getEmptyDashboard() throws Exception {
		
		DBBoxDTO box = new DBBoxDTO();
		box.setTemperature(new BigDecimal(0));
		box.setPressure( new BigDecimal(0) );
		box.setLatitude( new BigDecimal(0) );
		box.setLongitude( new BigDecimal(0) );
		
		Map<String, Object> dashMap = new LinkedHashMap<String, Object>();
		dashMap.put("temperature", box );
		dashMap.put("pressure", box );
		dashMap.put("latitude", box );
		dashMap.put("longitude", box );
		
		return dashMap;
	}
	
}
