package com.cubelink.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "forumreplies")
public class ForumReplies {

	/**
	 * 
	 */
	protected static final int ALLOC_SIZE=1;
	//private static final long serialVersionUID = 1L;
	public static final String SEQUENCE_NAME="replies_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="replies_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="replies_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long seqId;
	
	@Column (name="sreply", nullable=false,updatable=true,insertable=true, length=500)
	private String forumReply;
	
	@Column (name="dcreatedt", nullable=false,updatable=true,insertable=true)
    private Date createdDt;
	
	@Column (name="lforumid", nullable=false)
	private Long forumId;
	
	@Column (name="lauthorid", nullable=false,updatable=true,insertable=true)
	private Long authorId;
	
	@Column (name="sauthorname", nullable=false,updatable=true,insertable=true)
	private String authorName;
	
	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public String getForumReply() {
		return forumReply;
	}

	public void setForumReply(String forumReply) {
		this.forumReply = forumReply;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public Long getForumId() {
		return forumId;
	}

	public void setForumId(Long forumId) {
		this.forumId = forumId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

}
