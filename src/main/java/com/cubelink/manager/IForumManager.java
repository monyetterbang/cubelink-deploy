package com.cubelink.manager;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.cubelink.dto.ForumDTO;
import com.cubelink.dto.ForumRepliesDTO;
import com.cubelink.entity.Forum;
import com.cubelink.entity.ForumReplies;

public interface IForumManager {

	public List<Forum> listForum();
	public List<ForumReplies> listReplies(Long id);	
	
	public ForumDTO createNewForum (ForumDTO f, HttpSession s) throws Exception;
	public ForumRepliesDTO createNewReply(ForumRepliesDTO f, Long forumId, HttpSession s)throws Exception;
}
