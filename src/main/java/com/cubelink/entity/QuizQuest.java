package com.cubelink.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "quizquest")
public class QuizQuest {

	/**
	 * 
	 */
	protected static final int ALLOC_SIZE=1;
	public static final String SEQUENCE_NAME="quizquest_seq";
	
	@Id
	@Column (name="lseqid", nullable=false)
	@GeneratedValue(generator="quizquest_seq", strategy=GenerationType.SEQUENCE )
	@SequenceGenerator(name="quizquest_seq", sequenceName=SEQUENCE_NAME, allocationSize=ALLOC_SIZE)
	private Long seqId;

	@Column (name="dcreatedt", updatable=true, insertable=true)
    private Date createdDt;
	
	@Column (name="squestion", updatable=true, insertable=true)
    private String quizQestion;
	
	@Column (name="sanswer", updatable=true, insertable=true)
    private String quizAnswer;
	
	@Column (name="lquizid", updatable=false, insertable=false)
    private Long quizId;

//	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
//	@JoinColumn(name = "lquizid")
//	private Quiz quiz;
	
	public Long getSeqId() {
		return seqId;
	}

	public void setSeqId(Long seqId) {
		this.seqId = seqId;
	}

	public Date getCreatedDt() {
		return createdDt;
	}

	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}

	public String getQuizQestion() {
		return quizQestion;
	}

	public void setQuizQestion(String quizQestion) {
		this.quizQestion = quizQestion;
	}

	public String getQuizAnswer() {
		return quizAnswer;
	}

	public void setQuizAnswer(String quizAnswer) {
		this.quizAnswer = quizAnswer;
	}

	public Long getQuizId() {
		return quizId;
	}

	public void setQuizId(Long quizId) {
		this.quizId = quizId;
	}

//	public Quiz getQuiz() {
//		return quiz;
//	}
//
//	public void setQuiz(Quiz quiz) {
//		this.quiz = quiz;
//	}
	
	
	
	
	
}
