package com.cubelink.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.ForumDTO;
import com.cubelink.entity.Forum;

@Mapper
public interface ForumMapper {

	public ForumMapper INSTANCE = Mappers.getMapper(ForumMapper.class);

	public ForumDTO ForumToForumDTO( Forum m );
	
	public ForumDTO ForumToForumDTO( Optional<Forum> m );
	
	public Forum ForumDTOtoForum( ForumDTO m );
	
	public void updateForum( Forum user, @MappingTarget Forum user1 );

	public void updateForum (ForumDTO mDTO, @MappingTarget Forum m );
}
