package com.cubelink.dto;

import org.springframework.http.HttpStatus;

public class ResponseDTO {
	private Integer code;
	private HttpStatus status;
	private String message;
	private Object returnObj;
	
	public ResponseDTO() {
		
	}
	public ResponseDTO ( HttpStatus status, String message ) {
		this.status = status;
		this.message = message;
	}	
	public Integer getCode() {
		this.code = status.value();
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getReturnObj() {
		return returnObj;
	}
	public void setReturnObj(Object returnObj) {
		this.returnObj = returnObj;
	}
}
