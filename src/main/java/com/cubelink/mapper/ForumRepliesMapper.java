package com.cubelink.mapper;

import java.util.Optional;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.cubelink.dto.ForumRepliesDTO;
import com.cubelink.entity.ForumReplies;

@Mapper
public interface ForumRepliesMapper {
	
	public ForumRepliesMapper INSTANCE = Mappers.getMapper(ForumRepliesMapper.class);

	public ForumRepliesDTO ForumRepliesToForumRepliesDTO( ForumReplies m );
	
	public ForumRepliesDTO ForumRepliesToForumRepliesDTO( Optional<ForumReplies> m );
	
	public ForumReplies ForumRepliesDTOtoForumReplies( ForumRepliesDTO m );
	
	public void updateForumReplies( ForumReplies user, @MappingTarget ForumReplies user1 );

	public void updateForumReplies (ForumRepliesDTO mDTO, @MappingTarget ForumReplies m );
}
