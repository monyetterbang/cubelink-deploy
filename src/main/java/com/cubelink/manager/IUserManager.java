package com.cubelink.manager;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cubelink.dto.UserByAgeDTO;
import com.cubelink.dto.UserByLevelDTO;

//import org.springframework.data.domain.Page;

import com.cubelink.dto.UserDTO;
import com.cubelink.entity.UserMaster;
import com.cubelink.exception.RecordNotFoundException;

public interface IUserManager {

	//public Page<UserDetailDTO> retrieveAllUsers( Optional<Integer> optPage, Optional<Integer> optSize );
	public UserDTO retrieveUserById(  Long id ) throws RecordNotFoundException, Exception;
	public UserDTO retrieveByUserNamePass( String userName, String pass ) throws Exception;
	public Boolean authenticate( Long userId,  String password );
	public List<UserDTO> findByExample( UserMaster user );	
	
	public Boolean isUserExist( Long seqId ) throws Exception;
	public Boolean authenticateUser( String username,  String password );
	

	//added 27Feb after changing scmuser
	public UserDTO updateUser( UserDTO contactDTO ) throws Exception;
	public UserDTO retrieveContactById( Optional<Long> id ) throws RecordNotFoundException, Exception;
	
	public Boolean isUserNotExistByUsername( String username ) throws Exception;
	public Date getCurrDate() throws Exception;

	public UserByAgeDTO findAllByAge(UserByAgeDTO u);	
	public UserByLevelDTO findAllByLevel(UserByLevelDTO u); 
	
	public Optional<UserMaster> retrieveByUserNP( String userName, String pass ) throws Exception;
	
	public  List<UserMaster> getMostActiveUser();	
	
}
